void add_photon_tests() {
    Test.add_func("/photon/utils/seconds_to_string", () => {
        assert_true(Photon.seconds_to_string(10) == "0\u223610");
        assert_true(Photon.seconds_to_string(100) == "1\u223640");
        assert_true(Photon.seconds_to_string(1000) == "16\u223640");
        assert_true(Photon.seconds_to_string(10000) == "2\u223646\u223640");
        assert_true(Photon.seconds_to_string(100000) == "27\u223646\u223640");
    });

    Test.add_func("/photon/config", () => {
        var config = Photon.Config.get_instance();

        assert_true(config.dark_theme == true);
        assert_true(config.framerate == 60);
        assert_true(config.crossfade_duration == 0.25);
        assert_true(config.thumbnail_aspect == 16.0/9);
        assert_true(config.thumbnail_width == 160);
        assert_true(config.volume == 100);
        assert_true(config.volume_mute == false);
        assert_true(config.copy_on_import == true);
        assert_true(config.import_lines_per_slide == 4);
        assert_true(config.show_import_suggestions == true);
        assert_true(config.debugmode == false);
        assert_true(config.remotes_enabled == false);
        assert_true(config.check_for_updates == true);
    });

    Test.add_func("/photon/config-file", () => {
        var config = new Photon.ConfigFile("not-a-real-file");

        assert_true(config.get_int("int") == 0);
        assert_true(config.get_bool("bool") == false);
        assert_true(config.get_double("double") == 0.0);
        assert_true(config.get_string("string") == null);

        config.set_int("int", 10);
        config.set_bool("bool", true);
        config.set_double("double", 11.75);
        config.set_string("string", "I'M A STRING");

        assert_true(config.get_int("int") == 10);
        assert_true(config.get_bool("bool") == true);
        assert_true(config.get_double("double") == 11.75);
        assert_true(config.get_string("string") == "I'M A STRING");

        config.set_int("int", 20, "section");
        config.set_bool("bool", true, "section");
        config.set_double("double", 1.25, "section");
        config.set_string("string", "striiiiiing", "section");

        assert_true(config.get_int("int", 0, "section") == 20);
        assert_true(config.get_bool("bool", false, "section") == true);
        assert_true(config.get_double("double", 0, "section") == 1.25);
        assert_true(config.get_string("string", null, "section") == "striiiiiing");

        assert_true(config.get_int("nonexistant", 100) == 100);
        assert_true(config.get_bool("nonexistant", true) == true);
        assert_true(config.get_double("nonexistant", 10.5) == 10.5);
        assert_true(config.get_string("nonexistant", "oof") == "oof");

        assert_true(config.get_int("nonexistant", 100, "section") == 100);
        assert_true(config.get_bool("nonexistant", true, "section") == true);
        assert_true(config.get_double("nonexistant", 10.5, "section") == 10.5);
        assert_true(config.get_string("nonexistant", "oof", "section") == "oof");
    });

    Test.add_func("/photon/utils/duplicate_json", () => {
        Json.Object json = new Json.Object();
        json.set_string_member("string", "TEST STRING");
        json.set_double_member("double", 3.1415);
        var sub_obj = new Json.Object();
        var array = new Json.Array();
        array.add_boolean_element(false);
        array.add_boolean_element(true);
        sub_obj.set_array_member("array", array);
        json.set_object_member("object", sub_obj);

        Json.Object copy = Photon.duplicate_json(json);

        // make sure it didn't hand us back the original
        assert_true(json != copy);

        // make sure it's all the same
        assert_true(copy.get_string_member("string") == "TEST STRING");
        assert_true(copy.get_double_member("double") == 3.1415);
        var copy_sub_obj = copy.get_object_member("object");
        assert_true(copy_sub_obj != sub_obj);
        var copy_array = copy_sub_obj.get_array_member("array");
        assert_true(copy_array != array);
        assert_false(copy_array.get_boolean_element(0));
        assert_true(copy_array.get_boolean_element(1));
    });
}
