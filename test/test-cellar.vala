void add_cellar_tests() {
    Test.add_func("/cellar/first-load", () => {
        /* Test that Cellar loads properly the first time. */
        Cellar.init();

        Cellar.Library.get_instance();

        var playlists = Cellar.PlaylistIndex.get_instance();

        var entry = playlists.get_playlist_entry(Photon.DEFAULT_PLAYLIST);
        assert_nonnull(entry);
        assert_true(playlists.open_playlist == entry);

        var playlist = entry.get_playlist();

        var segments = playlist.get_segments();
        assert_true(segments.size > 0);

        var segment = segments[0];
        assert_true(segment.slideshow.arrangement.get_slides().size > 0);
    });

    Test.add_func("/cellar/playlist-index", () => {
        /* Test basic operations on the playlist index. */
        var playlists = Cellar.PlaylistIndex.get_instance();

        var playlist1 = new Cellar.PlaylistEntry.from_scratch("TEST PLAYLIST");
        playlists.add_playlist(playlist1);
        assert_true(playlists.get_playlists().size == 2);
        assert_true(playlists.get_playlist_entry(playlist1.uuid) == playlist1);

        playlists.add_folder("TEST FOLDER");
        var folders = playlists.get_folders();
        assert_true(folders.size == 1);
        var folder = get_item_in_collection(folders.keys);

        playlists.rename_folder(folder, "TEST FOLDER RENAMED");
        assert_true(get_item_in_collection(folders.values) == "TEST FOLDER RENAMED");

        // make sure folder names must be unique
        assert_false(playlists.is_valid_folder_name("TEST FOLDER RENAMED"));

        playlist1.folder = folder;
        playlists.remove_folder(folder);

        // make sure playlist is now in the trash
        assert_true(playlist1.trash);
        assert_true(folders.size == 0);

        assert_null(playlist1.folder);

        // make sure empty_trash does not delete things not in trash
        playlist1.trash = false;
        playlists.empty_trash();
        assert_true(playlists.get_playlists().size == 2);

        // make sure empty_trash does delete things in the trash
        playlist1.trash = true;
        playlists.empty_trash();
        assert_true(playlists.get_playlists().size == 1);

        // make sure the open playlist can't be deleted
        var playlist2 = playlists.get_playlist_entry(Photon.DEFAULT_PLAYLIST);
        assert_true(playlists.open_playlist == playlist2);
        playlists.delete_playlist(playlist2);
        // make sure it still exists
        assert_true(playlists.get_playlists().size == 1);
        assert_nonnull(playlists.get_playlist_entry(Photon.DEFAULT_PLAYLIST));
    });

    Test.add_func("/cellar/slideshow-resource", () => {
        /* Test that slideshow resources can be added to the library. */
        var library = Cellar.Library.get_instance();

        var res = new Cellar.Resource.from_scratch(
            Cellar.ResourceType.SLIDESHOW,
            "TEST SLIDESHOW",
            null
        );
        library.user_shelf.add_resource(res);

        assert_true(res == library.get_resource(res.uuid));
        assert_true(res.name == "TEST SLIDESHOW");

        var slideshow = res.slideshow;
        assert_nonnull(slideshow);
        assert_true(slideshow.arrangement.get_slides().size == 0);
    });

    Test.add_func("/cellar/slideshow-operations", () => {
        /* Test that basic slideshow, arrangement, slide, and group
         * operations work */

        var slide = new Cellar.Slide.for_lyrics("TEST LYRICS");
        assert_true(slide.get_field("content") == "TEST LYRICS");

        var slideshow = new Cellar.Slideshow.from_scratch();
        var arrangement = slideshow.arrangement;

        var slides = arrangement.get_slides();
        assert_true(slides.size == 0);

        var slide1 = new Cellar.Slide.blank();
        var slide2 = new Cellar.Slide.blank();
        var slide3 = new Cellar.Slide.blank();

        arrangement.insert_slide(slide1);
        assert_true(slides.size == 1);

        arrangement.insert_slide(slide2);
        arrangement.insert_slide(slide3, 1);
        assert_true(slides.size == 3);
        assert_true(slides[0] == slide1);
        assert_true(slides[1] == slide3);
        assert_true(slides[2] == slide2);

        var group1 = new Cellar.Group.from_scratch(slideshow);
        var group_slides = group1.get_slides();
        assert_true(group_slides.size == 1);

        var slide10 = group_slides[0];
        group1.remove_slide(group_slides[0]);
        assert_true(group_slides.size == 1); // make sure the group has the default slide
        assert_true(group_slides[0] != slide10); // make sure slide was deleted and a new one created

        var slide11 = new Cellar.Slide.blank();
        var slide12 = new Cellar.Slide.blank();
        var slide13 = new Cellar.Slide.blank();
        group1.insert_slide(slide11);
        group1.insert_slide(slide12);
        group1.insert_slide(slide13, 0);
        group1.remove_slide(group_slides[1]);
        assert_true(group_slides.size == 3);
        assert_true(group_slides[0] == slide13);
        assert_true(group_slides[1] == slide11);
        assert_true(group_slides[2] == slide12);

        assert_true(group1.is_first_slide(slide13));

        arrangement.insert_group(group1);
        assert_true(slides.size == 6);
        assert_true(arrangement.get_contents().size == 4);

        arrangement.remove_content(0);
        assert_true(slides.size == 5);
        assert_true(arrangement.get_contents().size == 3);

        arrangement.remove_content(2);
        assert_true(slides.size == 2);
        assert_true(arrangement.get_contents().size == 2);
    });

    Test.add_func("/cellar/subresources", () => {
        /* Test that subresources work */

        var library = Cellar.Library.get_instance();

        var res = new Cellar.Resource.from_scratch(
            Cellar.ResourceType.SLIDESHOW,
            "TEST SLIDESHOW",
            null
        );
        library.user_shelf.add_resource(res);

        var subres = new Cellar.Resource.from_scratch(
            Cellar.ResourceType.SLIDESHOW,
            "TEST SLIDESHOW (SUBRESOURCE)",
            null
        );
        res.add_subresource(subres);

        var subres2 = new Cellar.Resource.from_scratch(
            Cellar.ResourceType.SLIDESHOW,
            "TEST SLIDESHOW (SUBRESOURCE x2)",
            null
        );
        subres.add_subresource(subres2);

        string full_id = @"$(res.uuid)/$(subres.uuid)/$(subres2.uuid)";

        assert_true(res.get_subresource(subres.uuid) == subres);
        assert_true(library.get_resource(subres2.get_full_id()) == subres2);
        assert_true(subres2.get_full_id() == full_id);

        // subresources should not be directly accessible
        assert_null(library.get_resource(subres.uuid));
        assert_null(library.get_resource(subres2.uuid));
    });
}
