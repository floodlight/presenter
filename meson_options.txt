option(
  'packaged',
  type: 'boolean',
  value: false,
  description: 'Create a "packaged" application. On macOS, creates an app bundle. On Windows, creates an install directory.'
)
