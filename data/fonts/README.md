Cantarell is licensed under the SIL Open Font License, version 1.1. See the
SIL1.1 file for a copy of that license.

The source for Cantarell is located at
<https://gitlab.gnome.org/GNOME/cantarell-fonts>.
