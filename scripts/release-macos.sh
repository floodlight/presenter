#!/bin/bash

# Sets up Meson to build a macOS .app bundle for Floodlight. The build
# directory will be _mac_build and the app file will be
# _release/Floodlight.app.

# Typical usage:
#     ./scripts/release-macos.sh
#     ninja -C _mac_build install
#     open _release/Floodlight.app

meson _mac_build \
  --prefix $PWD/_release/Floodlight.app \
  --bindir Contents/MacOS \
  --libdir Contents/Frameworks \
  --datadir Contents/Resources \
  -Dpackaged=true
