#!/bin/bash

# Set up bundle structure
bundle=$MESON_INSTALL_PREFIX
executable=$bundle/bin/floodlight-presenter.exe
resources=$bundle/resources
libraries=$bundle/bin
# Make sure directories exist
mkdir -p $resources
mkdir -p $libraries

PKGCONFIG=pkg-config
MINGW=/mingw64
QUERY_PIXBUF=gdk-pixbuf-query-loaders
LDD="$MESON_SOURCE_ROOT/src/build/mingw-ldd.py"

# If cross-compiling, make sure to use the right $PKGCONFIG
if [ "$1" == "cross" ] ; then
  PKGCONFIG="$2"
  MINGW="$3"
  QUERY_PIXBUF="wine $MINGW/bin/gdk-pixbuf-query-loaders.exe"
  shift 3
fi

# Functions to copy necessary libraries and modify their paths
copied=""
copy_and_fix () {
  name=$(basename $1)
  dest=$libraries/$name

  # do not copy a library twice
  if echo "$copied" | grep -q "$name" ; then return; fi
  copied="$copied $name"

  echo "Copying $name"
  cp $1 $dest
  chmod u+w $dest
  fix "$dest"
}

fix () {
  deps="`$LDD "$1" "$MINGW/bin" | sed -rn 's|.*=> (.+)|\1|p' | grep '/mingw'`"
  for dep in $deps; do
    copy_and_fix "$dep"
  done
}


# Copy all necessary libraries into the bundle
fix $executable

# Copy GStreamer plugins from several different packages
mkdir -p $libraries/gst-plugins
gst_plugins=$($PKGCONFIG --variable=libdir gstreamer-1.0)/gstreamer-1.0
for file in $gst_plugins/libgst*.dll; do
  copy_and_fix $file
done

# copy this gstreamer utility
gst_plugin_scanner=$($PKGCONFIG --variable=prefix gstreamer-1.0)/libexec/gstreamer-1.0/gst-plugin-scanner.exe
copy_and_fix $gst_plugin_scanner


# Some prefixes we need to copy stuff from
gtk_prefix=$($PKGCONFIG --variable=prefix gtk+-3.0)
gtk_immodules=$gtk_prefix/lib/gtk-3.0/$($PKGCONFIG --variable=gtk_binary_version gtk+-3.0)/immodules
gdk_pixbuf_prefix=$($PKGCONFIG --variable=prefix gdk-pixbuf-2.0)
gdk_loaders=$gdk_pixbuf_prefix/lib/gdk-pixbuf-2.0/$($PKGCONFIG --variable=gdk_pixbuf_binary_version gdk-pixbuf-2.0)/loaders

# Copy gdk-pixbuf loaders
for file in $gdk_loaders/libpixbufloader-*.dll; do
  copy_and_fix $file
done
# generate and fix gdk-loaders.cache
$QUERY_PIXBUF \
  | sed -r 's|"[^"]+(libpixbufloader-.+\.dll)"|"bin/\1"|g' > $resources/gdk-loaders.cache

# # Copy gtk immodules
# for file in $gtk_immodules/im-*.dll; do
#   copy_and_fix $file
# done
# # generate and fix immodules.cache
# gtk-query-immodules-3.0 \
#   | sed "s|$gtk_immodules|@rpath|g" \
#   | sed "s|$gtk_prefix|@executable_path/../Resources|g" \
#   > $resources/immodules.cache
# copy locale data. for now, only copy english data, because that's the only
# language Floodlight itself supports
mkdir -p $resources/share/locale
cp -R $gtk_prefix/share/locale/en* $resources/share/locale

# Copy icons
# TODO: Copy only the files we need, instead of copying and then deleting
icon_dir=$MINGW/share/icons/Adwaita
mkdir -p $resources/share/icons
cp -R -L $icon_dir $resources/share/icons/
# delete non-symbolic icons, which we don't use
find $resources/share/icons/Adwaita/* -type f ! -name "*symbolic.*" -a -name "*.png" -delete
# delete cursors directory, it's huge and doesn't seem to be important
rm -r $resources/share/icons/Adwaita/cursors
# For some reason, copying the hicolor theme's index.theme file fixes a couple
# missing icons, even though hicolor doesn't actually contain any icons
mkdir -p $resources/share/icons/hicolor
cp $icon_dir/../hicolor/index.theme $resources/share/icons/hicolor/index.theme

# GLib schemas
mkdir -p $resources/share/glib-2.0/schemas
glib-compile-schemas --targetdir=$resources/share/glib-2.0/schemas $gtk_prefix/share/glib-2.0/schemas

# MIME database
mkdir $resources/mime
cp $(pkg-config --variable=prefix shared-mime-info)/share/mime/mime.cache $resources/mime

# Create InstallFloodlight.exe
makensis $bundle/windows-installer.nsi
