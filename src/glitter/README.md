# Glitter: Floodlight's User Interface

Glitter is the module containing all the code behind the Presenter Console. It
uses the UI definitions found in `data/ui` and passes user commands to the
other modules.
