namespace Glitter {
    public class Thumbnails : Object {
        public const string THUMBNAIL_QUARK = "floodlight-thumbnail-task";


        /**
         * Sets the image to a thumbnail of the given slide.
         *
         * To prevent jumpiness or freezing in the UI, a blank thumbnail will
         * be set immediately, then the thumbnail will be set when it is done
         * rendering (in another thread).
         *
         * If `w` or `h` are 0, they will be inferred from the other argument
         * and the default aspect ratio. If they are both zero, the width will be
         * taken from the default.
         *
         * If `slide` is null, the first slide of the segment will be used. If the
         * segment is empty, a solid transparent surface will be returned.
         *
         * The slide background is taken from `segment`. If `segment` is null
         * (and `slide` is not), the slide will simply be drawn without a
         * background.
         */
        public static void thumbnail_slide(Gtk.Image image,
                                           Cellar.Segment? segment,
                                           Cellar.Slide? slide,
                                           int w=0, int h=0) {

            if (segment == null && slide == null) {
                warning("Cannot call thumbnail_slide with no segment or slide!");
                return;
            }

            add_thumbnail_task(new ThumbnailTask(image, segment, slide), w, h);
        }

        public static void thumbnail_resource(Gtk.Image image,
                                              Cellar.Resource resource,
                                              int w=0, int h=0) {

            add_thumbnail_task(new ThumbnailTask.for_resource(image, resource), w, h);
        }

        /**
         * Gets a placeholder thumbnail at the current size.
         */
        public static Cairo.Surface get_placeholder() {
            int w = Photon.Config.get_instance().thumbnail_width;
            int h = (int) (w / Photon.Config.get_instance().thumbnail_aspect);

            var placeholder = get_instance().placeholder;
            if (placeholder == null
                || placeholder.get_width() != w
                || placeholder.get_height() != h) {

                get_instance().placeholder = new Cairo.ImageSurface(
                    Cairo.Format.ARGB32, w, h
                );
            }

            return get_instance().placeholder;
        }

        private static void add_thumbnail_task(ThumbnailTask task, int w, int h) {
            Gtk.Image image = task.image;

            if (image.get_data<ThumbnailTask>(THUMBNAIL_QUARK) != null) {
                image.get_data<ThumbnailTask>(THUMBNAIL_QUARK).canceled = true;
            }

            if (h == 0) {
                if (w == 0) w = Photon.Config.get_instance().thumbnail_width;
                h = (int) (w / Photon.Config.get_instance().thumbnail_aspect);
            } else if (w == 0) {
                w = (int) (h * Photon.Config.get_instance().thumbnail_aspect);
            }

            task.w = w;
            task.h = h;

            image.set_data(THUMBNAIL_QUARK, task);
            get_instance().queue.push(task);
        }


        private static Thumbnails instance;
        public static Thumbnails get_instance() {
            if (instance == null) instance = new Thumbnails();
            return instance;
        }


        /**
         * The thread to be used to create thumbnails.
         */
        private Thread thread;

        /**
         * Task queue.
         */
        private AsyncQueue<ThumbnailTask> queue;

        private Cairo.ImageSurface placeholder;


        private Thumbnails() {
            this.queue = new AsyncQueue<ThumbnailTask>();

            this.thread = new Thread<int>("thumbnails", () => {
                while (true) this.queue.pop().create_thumbnail();
            });
        }
    }


    /**
     * Represents a thumbnail that needs to be created in a separate thread.
     */
    private class ThumbnailTask {
        public int w;
        public int h;

        public Gtk.Image image { get; private set; }

        /**
         * Set to true if the image widget gets a later ThumbnailTask.
         */
        public bool canceled = false;


        private Cellar.Segment? segment;
        private Cellar.Slide? slide;

        private Cellar.Resource? resource;


        public ThumbnailTask(Gtk.Image image, Cellar.Segment? segment, Cellar.Slide? slide) {
            this.image = image;
            this.segment = segment;
            this.slide = slide;
        }

        public ThumbnailTask.for_resource(Gtk.Image image, Cellar.Resource resource) {
            this.image = image;
            this.resource = resource;
        }


        /**
         * Creates the thumbnail and passes it to the `finished` signal.
         */
        public void create_thumbnail() {
            if (this.canceled) return;
            // cancel if the image is no longer part of the widget tree
            if (this.image.parent == null) return;

            Cairo.ImageSurface surf = null;
            if (this.resource != null) surf = this.create_thumbnail_resource();
            else surf = this.create_thumbnail_slide();

            if (surf == null) {
                surf = new Cairo.ImageSurface(Cairo.Format.ARGB32, this.w, this.h);
            }

            Idle.add(() => {
                image.set_from_surface(surf);
                image.set_data(Thumbnails.THUMBNAIL_QUARK, null);
                return Source.REMOVE;
            });
        }


        private Cairo.ImageSurface? create_thumbnail_resource() {
            if (this.resource.thumbnail == null) return null;
            
            try {
                var pixbuf = new Gdk.Pixbuf.from_file(this.resource.thumbnail.get_path());
            
                var surf = new Cairo.ImageSurface(Cairo.Format.ARGB32, this.w, this.h);
                var cr = new Cairo.Context(surf);
                Halogen.draw_pixbuf_to_context(
                    cr, pixbuf, 0, 0, this.w, this.h,
                    new Halogen.DrawOpts.from_resource(this.resource), 1, true
                );

                return surf;
            } catch (Error e) {
                warning("Could not load image thumbnail: %s", e.message);
                debug("For resource %s (%s)", this.resource.uuid, this.resource.thumbnail.get_path());
                return null;
            }
        }

        private Cairo.ImageSurface? create_thumbnail_slide() {
            // For now, just draw the thumbnail of the primary feed. This might
            // be improved later, to include all feeds that aren't stage views.

            Cellar.Slide actual_slide = slide;
            if (actual_slide == null) {
                Gee.List<Cellar.Slide> slides = segment.slideshow.arrangement.get_slides();
                if (slides.size > 0) {
                    actual_slide = slides[0];
                } else {
                    // return a blank surface
                    return null;
                }
            }

            Halogen.LayoutRenderer renderer = Halogen.LayoutManager.get_instance()
                                              .get_layout(actual_slide.layout_id)
                                              .page_for_feed("primary")
                                              .get_thumbnail_renderer(segment, actual_slide);

            var surf = new Cairo.ImageSurface(Cairo.Format.ARGB32, w, h);
            var cr = new Cairo.Context(surf);
            renderer.render(cr, 0, 0, w, h);

            return surf;
        }
    }
}
