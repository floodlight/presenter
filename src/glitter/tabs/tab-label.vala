namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/tab-label.ui")]
    public class TabLabel : Gtk.Box {
        [GtkChild] private unowned Gtk.Label label;
        [GtkChild] private unowned Gtk.Image icon;


        private Tab tab;


        public TabLabel(Tab tab) {
            this.tab = tab;

            this.tab.notify["label"].connect(() => {
                this.label.label = this.tab.label;
            });
            this.label.label = this.tab.label;

            string icon_name = tab.tab_info.get_icon_name();
            if (icon_name != null) {
                this.icon.set_from_icon_name(icon_name, Gtk.IconSize.BUTTON);
            } else {
                this.icon.hide();
            }
        }


        [GtkCallback]
        private void on_close_clicked() {
            PresenterConsole.get_instance().tabs.close_tab(this.tab);
        }
    }
}
