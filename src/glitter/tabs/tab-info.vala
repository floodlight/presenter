namespace Glitter {
    public enum TabType {
        PREFERENCES,
        HELP,
        ABOUT,
        EDIT_SLIDESHOW,
        EDIT_MEDIA,
        UPDATES
    }

    public class TabInfo : Object {
        /**
         * The tab type.
         */
        public TabType tab_type { get; private set; }

        /**
         * The path that is being displayed. Tabs with different paths are
         * considered different tabs.
         */
        public string? path { get; private set; }

        /**
         * The subpath. The difference between this and `path` is that opening
         * a new tab with the same path but different subpath will not open a
         * new tab; it will set the subpath of the existing tab.
         */
        public string? subpath { get; set; }


        public TabInfo(TabType tab_type, string? path, string? subpath=null) {
            this.tab_type = tab_type;
            this.path = path;
            this.subpath = subpath;
        }

        /**
         * Creates a TabInfo, either EDIT_SLIDESHOW or EDIT_MEDIA, for the
         * given resource.
         */
        public TabInfo.for_resource_editor(Cellar.Resource resource) {
            TabType type = resource.resource_type.is_slideshow()
                ? TabType.EDIT_SLIDESHOW
                : TabType.EDIT_MEDIA;
            this(type, resource.get_full_id());
        }


        /**
         * Determines whether this tabinfo and another should be considered the
         * same tab.
         */
        public bool matches(TabInfo that) {
            return (this.tab_type == that.tab_type) && (this.path == that.path);
        }

        /**
         * Creates the tab described by the TabInfo.
         */
        public Tab? create_tab() {
            switch (this.tab_type) {
                case PREFERENCES:
                    return new PreferencesTab(this);
                case ABOUT:
                    return new AboutTab(this);
                case EDIT_SLIDESHOW:
                    return new SlideshowEditorTab(this);
                case EDIT_MEDIA:
                    return new MediaEditorTab(this);
                case UPDATES:
                    return new UpdatesTab(this);
                default:
                    return null;
            }
        }

        /**
         * Gets the icon name based on the tab type. `null` if there should be
         * no icon.
         */
        public string? get_icon_name() {
            switch (this.tab_type) {
                case PREFERENCES:
                    return "preferences-system-symbolic";
                case HELP:
                    return "help-browser-symbolic";
                case ABOUT:
                    return "preferences-system-details-symbolic";
                case EDIT_SLIDESHOW:
                case EDIT_MEDIA:
                    return "document-edit-symbolic";
                case UPDATES:
                    return "software-update-available-symbolic";
                default:
                    return null;
            }
        }
    }
}
