namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/bottom-tabs.ui")]
    public class BottomTabs : Gtk.Notebook {
        [GtkChild] private unowned Gtk.Stack icon_stack;


        /**
         * The position of the split pane when the tabs are expanded.
         */
        private int expanded_pos;

        private bool collapsed = true;


        construct {
            /* Glade doesn't like it when a notebook has no pages, so there's
             * a dummy tab in the UI file. Remove it here. */
            this.remove_page(0);

            // Open an editor tab when a resource is added
            Cellar.Library.get_instance().resource_added.connect((res) => {
                // Not for subresources--you access those through the parent
                // resource
                if (res.parent == null) {
                    var info = new TabInfo.for_resource_editor(res);
                    this.show_tab(info);
                }
            });
        }


        /**
         * Shows the tab described by the TabInfo, opening a new tab if needed.
         */
        public void show_tab(TabInfo info) {
            bool exists_already = this.foreach_tab((tab) => {
                if (tab.tab_info.matches(info)) {
                    Gtk.Widget child = this.get_notebook_child(tab);
                    tab.tab_info.subpath = info.subpath;
                    if (this.page != -1) this.set_current_page(this.page_num(child));
                    this.expand_tabs();
                    return true;
                }
                return false;
            });
            if (exists_already) return;

            Tab tab = info.create_tab();
            TabLabel label = new TabLabel(tab);

            var revealer = new Gtk.Revealer();
            revealer.show();
            revealer.reveal_child = true;
            // prevent double clicking in the tab area from reaching the
            // handler that collapses the tab view. that way, it only activates
            // when the tab bar itself is clicked
            revealer.button_press_event.connect(() => true);

            // The scrolled window is required for tabs to collapse properly
            // I'm not sure why
            var scroller = new Gtk.ScrolledWindow(null, null);
            revealer.add(scroller);
            revealer.set_transition_duration(0);
            scroller.add(tab);
            scroller.get_style_context().add_class("bg-background");
            scroller.show();

            int index = this.append_page(revealer, label);
            this.set_current_page(index);
            this.expand_tabs();
        }

        public void close_tab(Tab tab) {
            var widget = this.get_notebook_child(tab);
            if (widget == null) return;

            this.remove(widget);
            if (this.get_n_pages() == 0) this.collapse_tabs();
        }

        /**
         * Creates a JSON representation of the currently open tabs. Used for
         * saving UI state.
         */
        public Json.Object save_tabs() {
            Json.Object json = new Json.Object();
            Json.Array tabs = new Json.Array();

            this.foreach_tab((tab) => {
                Json.Object tab_json = new Json.Object();
                tab_json.set_int_member("type", (int) tab.tab_info.tab_type);
                tab_json.set_string_member("path", tab.tab_info.path);
                tab_json.set_string_member("subpath", tab.tab_info.subpath);
                tabs.add_object_element(tab_json);
                return false;
            });

            json.set_array_member("tabs", tabs);
            json.set_int_member("open-tab", this.page);
            json.set_int_member("expanded-pos", this.expanded_pos);
            json.set_boolean_member("collapsed", this.collapsed);

            return json;
        }

        /**
         * Opens the tabs saved in the given JSON from save_tabs(). Used for
         * restoring UI state.
         */
        public void restore_tabs(Json.Object json) {
            // Doing this in an idle callback fixes a bug where the pane
            // position changes when the window is shown, causing the tabs
            // to get smaller with every save/restore cycle
            Idle.add(() => {
                this.expanded_pos = (int) json.get_int_member("expanded-pos");

                json.get_array_member("tabs").foreach_element((arr, index, val) => {
                    Json.Object tab_json = val.get_object();
                    TabInfo info = new TabInfo(
                        (TabType) tab_json.get_int_member("type"),
                        tab_json.get_string_member("path"),
                        tab_json.get_string_member("subpath")
                    );
                    this.show_tab(info);
                });

                if (this.get_n_pages() != 0) {
                    this.page = (int) json.get_int_member("open-tab");
                }

                if (json.get_boolean_member("collapsed")) this.collapse_tabs();
                else this.expand_tabs();
                return Source.REMOVE;
            });
        }


        private delegate bool ForeachTabCb(Tab tab);
        /**
         * Helper function to iterate through the tabs.
         *
         * If the callback returns true, iteration will stop and the function
         * will return true.
         */
        private bool foreach_tab(ForeachTabCb cb) {
            for (int i = 0, n = this.get_n_pages(); i < n; i++) {
                Tab tab;

                Gtk.Revealer child1 = (Gtk.Revealer) this.get_nth_page(i);
                Gtk.ScrolledWindow child2 = (Gtk.ScrolledWindow) child1.get_child();
                Gtk.Widget child3 = child2.get_child();
                if (child3 is Tab) tab = (Tab) child3;
                else tab = (Tab) ((Gtk.Viewport) child3).get_child();

                bool result = cb(tab);
                if (result) return true;
            }
            return false;
        }

        private void collapse_tabs() {
            if (this.collapsed) return;

            this.collapsed = true;
            this.icon_stack.visible_child_name = "collapsed";

            Gtk.Paned pane = PresenterConsole.get_instance().right_split;
            this.expanded_pos = pane.position;

            for (int i = 0, n = this.get_n_pages(); i < n; i++) {
                ((Gtk.Revealer) this.get_nth_page(i)).reveal_child = false;
            }

            // squish the pane as far down as it can go
            pane.position = 100000000;
        }

        private void expand_tabs() {
            if (!this.collapsed) return;

            this.collapsed = false;
            this.icon_stack.visible_child_name = "expanded";

            Gtk.Paned pane = PresenterConsole.get_instance().right_split;

            if (this.expanded_pos == 0 || this.expanded_pos >= pane.max_position) {
                Gtk.Allocation alloc;
                pane.get_allocation(out alloc);
                this.expanded_pos = alloc.height - 350;
            }

            for (int i = 0, n = this.get_n_pages(); i < n; i++) {
                ((Gtk.Revealer) this.get_nth_page(i)).reveal_child = true;
            }

            pane.position = this.expanded_pos;
        }


        /**
         * Gets the direct child of the notebook that the tab is in.
         */
        private Gtk.Widget? get_notebook_child(Tab tab) {
            Gtk.Widget widget = tab;
            while (!(widget.parent is Gtk.Notebook)) {
                widget = widget.parent;
                if (widget == null) return null;
            }
            return widget;
        }


        [GtkCallback]
        private void on_collapse_button_clicked() {
            if (this.collapsed) this.expand_tabs();
            else this.collapse_tabs();
        }

        [GtkCallback]
        private void on_size_allocate() {
            Gtk.Paned pane = PresenterConsole.get_instance().right_split;
            if (pane.position < pane.max_position) {
                this.expanded_pos = pane.position;
                this.expand_tabs();
            }
        }

        [GtkCallback]
        private bool on_button_press_event(Gdk.EventButton event) {
            if (event.type == Gdk.EventType.@2BUTTON_PRESS) this.collapse_tabs();
            else this.expand_tabs();
            return false;
        }
    }
}
