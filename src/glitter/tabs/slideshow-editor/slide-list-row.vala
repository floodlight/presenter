namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/slideshow-editor/slide-list-row.ui")]
    public class SlideListRow : Gtk.ListBoxRow {
        /**
         * Emitted when a slide should be added to the group or arrangement
         * this slide is in.
         *
         * @pos is the position of the inserted slide relative to this one.
         * Must either be 0 (to insert before) or 1 (to insert after).
         */
        public signal void add_slide(Cellar.Slide slide, int pos);

        /**
         * Emitted when the slide should be merged with the slide above it,
         * if possible.
         *
         * The two slides must have the same layout, and they must be part of
         * the same group (or both not in a group).
         */
        public signal void merge_slide();

        public Cellar.Slide slide { get; private set; }


        [GtkChild] private unowned Gtk.Stack stack;
        [GtkChild] private unowned Gtk.TextView content;
        [GtkChild] private unowned Gtk.Image thumbnail;
        [GtkChild] private unowned Gtk.Label group_label;
        [GtkChild] private unowned SlideListRowMenu menu;
        [GtkChild] private unowned Gtk.Popover rename_group;
        [GtkChild] private unowned Gtk.Label rename_group_label;
        [GtkChild] private unowned Gtk.Entry rename_group_entry;


        public SlideListRow(Cellar.Slide slide) {
            this.slide = slide;

            if (this.slide.group != null) {
                this.slide.group.notify["name"].connect(this.on_group_name_changed);
                GroupColors.get_instance().bind_label_style(this.group_label, this.slide.group);
            } else {
                GroupColors.get_instance().set_label_style(this.group_label, null);
            }

            this.slide.changed.connect(this.rebuild_ui);
            this.rebuild_ui();

            this.menu.slide_list_row = this;
        }


        private void rebuild_ui() {
            var layout = Halogen.LayoutManager.get_instance().get_layout(this.slide.layout_id);

            if (layout.has_field("content")) {
                string content = this.slide.get_field("content") ?? "";
                if (content != this.content.buffer.text) {
                    this.content.buffer.changed.disconnect(this.on_content_changed);
                    this.content.buffer.text = content;
                    this.content.buffer.changed.connect(this.on_content_changed);
                }

                this.stack.set_visible_child_name("content");
            } else {
                Thumbnails.thumbnail_slide(
                    this.thumbnail, null, this.slide, 0, 70
                );
                this.stack.set_visible_child_name("thumbnail");
            }
        }

        private void on_group_name_changed() {
            var listbox = ((Gtk.ListBox) this.get_parent());
            listbox.invalidate_headers();
        }

        [GtkCallback]
        private void on_content_changed() {
            this.slide.set_field("content", this.content.buffer.text);
        }

        [GtkCallback]
        private bool on_button_press_event(Gdk.EventButton event) {
            if (this.slide.group == null) return false;

            if (event.button == Gdk.BUTTON_SECONDARY) {
                this.menu.popup_at_pointer(event);
                ((Gtk.ListBox) this.get_parent()).select_row(this);
            }

            return false;
        }

        [GtkCallback]
        private bool on_text_button_press(Gdk.EventButton event) {
            if (!this.content.is_focus) {
                int x, y;
                Gtk.TextIter iter;
                this.content.window_to_buffer_coords(TEXT, (int) event.x, (int) event.y, out x, out y);
                this.content.get_iter_at_location(out iter, x, y);
                this.content.buffer.place_cursor(iter);
            }

            return false;
        }

        public void on_delete_item_activated() {
            this.slide.group.remove_slide(this.slide);
        }

        public void on_new_slide_before_item_activated() {
            this.add_slide(new Cellar.Slide.blank(), 0);
        }

        public void on_new_slide_after_item_activated() {
            this.add_slide(new Cellar.Slide.blank(), 1);
        }

        public void on_rename_group_item_activated() {
            this.rename_group_entry.text = this.slide.group.name;
            this.rename_group.popup();
        }

        [GtkCallback]
        private void on_rename_activated() {
            this.slide.group.name = this.rename_group_entry.text;
            this.rename_group.popdown();
        }

        [GtkCallback]
        private void on_rename_clicked() {
            this.slide.group.name = this.rename_group_entry.text;
            this.rename_group.popdown();
        }

        [GtkCallback]
        private void on_rename_changed() {
            GroupColors.get_instance().set_label_style_from_string(
                this.rename_group_label, this.rename_group_entry.text
            );
        }

        [GtkCallback]
        private bool on_content_key_press(Gdk.EventKey event) {
            if (event.keyval == Gdk.Key.BackSpace) {
                if (this.content.buffer.cursor_position == 0) {
                    // backspace at beginning of buffer
                    this.merge_slide();
                }
            }
            return false;
        }

        [GtkCallback]
        private void on_populate_popup(Gtk.Widget widget) {
            if (!(widget is Gtk.Menu)) {
                warning("populate-popup: widget is not a Gtk.Menu!");
                return;
            }
            Gtk.Menu menu = (Gtk.Menu) widget;

            if (this.slide.group != null) {
                var slide_items = new SlideListRowMenu();
                slide_items.slide_list_row = this;
                var slide = new Gtk.MenuItem.with_label(_("Slide"));
                slide.submenu = slide_items;
                slide.show();
                menu.prepend(slide);

                var separator = new Gtk.SeparatorMenuItem();
                separator.show();
                menu.prepend(separator);
            }

            var split = new Gtk.MenuItem.with_label(_("Split Slide"));
            split.activate.connect(this.split_slide);
            split.show();
            menu.prepend(split);
        }

        /**
         * Splits the text slide into two based on the current position of the
         * cursor in the text area.
         */
        private void split_slide() {
            string content = this.content.buffer.text;
            int pos = this.content.buffer.cursor_position;
            pos = content.index_of_nth_char(pos);

            string content_a = content.slice(0, pos).chomp();
            string content_b = content.slice(pos, content.length).chug();

            this.slide.set_field("content", content_a);

            var new_slide = new Cellar.Slide.duplicate_of(this.slide);
            new_slide.set_field("content", content_b);
            this.add_slide(new_slide, 1);
        }
    }
}
