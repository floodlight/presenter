namespace Glitter {
    /**
     * A label representing a group or slide in the rearranger.
     */
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/slideshow-editor/group-label.ui")]
    public class GroupLabel : Gtk.Box {
        public Cellar.ArrangementContent content { get; private set; }
        public Cellar.Arrangement arrangement { get; private set; }


        [GtkChild] private unowned Gtk.Label name_label;


        public GroupLabel(Cellar.Arrangement arrangement, int content_index) {
            this.arrangement = arrangement;
            this.content = arrangement.get_contents()[content_index];

            if (this.content.group != null) {
                this.content.group.bind_property("name", this.name_label, "label", SYNC_CREATE);
                GroupColors.get_instance().bind_label_style(this, this.content.group);
            } else {
                int s_index = this.arrangement.content_to_slide_index(content_index);
                this.name_label.label = _("Slide %d").printf(s_index + 1);
                GroupColors.get_instance().set_label_style(this, null);
            }
        }


        [GtkCallback]
        private void on_remove_clicked() {
            this.arrangement.remove_content_by_val(this.content);
        }
    }
}
