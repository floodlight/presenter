namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/updates.ui")]
    public class UpdatesTab : Gtk.Box, Tab {
        public string label { get; protected set; default=_("Updates"); }
        public TabInfo tab_info { get; protected set; }


        [GtkChild] private unowned Gtk.Box update_list;
        [GtkChild] private unowned Gtk.Stack stack;


        public UpdatesTab(TabInfo tab_info) {
            this.tab_info = tab_info;
            Photon.UpdateCheck.get_instance().notify["update-available"].connect((s, p) => {
                this.rebuild_ui();
            });
            Photon.Config.get_instance().notify["check-for-updates"].connect((s, p) => {
                this.rebuild_ui();
            });
            this.rebuild_ui();
        }


        private void rebuild_ui() {
            var updates = Photon.UpdateCheck.get_instance();
            var config = Photon.Config.get_instance();

            if (!config.check_for_updates) {
                this.stack.visible_child_name = "check_disabled";
            } else {
                if (updates.update_available) {
                    this.stack.visible_child_name = "update_available";
                } else {
                    this.stack.visible_child_name = "up_to_date";
                }

                clear_widget(this.update_list);

                foreach (var entry in updates.changelog) {
                    this.update_list.add(new UpdateEntry(entry));
                }
            }
        }


        [GtkCallback]
        private void on_go_to_downloads_clicked() {
            try {
                Gtk.show_uri_on_window(null, Photon.UpdateCheck.DOWNLOADS_URL, Gdk.CURRENT_TIME);
            } catch (Error e) {
                // not much we can do
            }
        }

        [GtkCallback]
        private void on_enable_update_check_clicked() {
            Photon.Config.get_instance().check_for_updates = true;
        }
    }


    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/update-entry.ui")]
    public class UpdateEntry : Gtk.Box {
        public Photon.ChangelogEntry entry { get; private set; }

        [GtkChild] private unowned Gtk.Label version;
        [GtkChild] private unowned Gtk.Label date;
        [GtkChild] private unowned Gtk.Label notes;


        public UpdateEntry(Photon.ChangelogEntry entry) {
            this.entry = entry;

            this.version.label = entry.version;
            this.notes.label = entry.notes;

            string[] date_parts = entry.get_field("released").split("-");
            var date = new DateTime(new TimeZone.local(),
                int.parse(date_parts[0]), int.parse(date_parts[1]),
                int.parse(date_parts[2]), 0, 0, 0
            );
            this.date.label = date.format("%B %_d, %Y");
        }
    }
}
