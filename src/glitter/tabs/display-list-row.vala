namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/tabs/display-list-row.ui")]
    public class DisplayListRow : Gtk.ListBoxRow {
        [GtkChild] private unowned Gtk.Label index_label;
        [GtkChild] private unowned Gtk.Label info;
        [GtkChild] private unowned Gtk.ComboBox feed_chooser;


        private int index;


        public DisplayListRow(int index, Gtk.ListStore feeds) {
            this.index = index;
            this.feed_chooser.model = feeds;

            this.index_label.label = (index + 1).to_string();

            var monitor = Gdk.Display.get_default().get_monitor(index);
            if (Photon.Config.get_instance().debugmode) {
                info.label = "%d×%d (%d, %d) %s".printf(
                    monitor.geometry.width, monitor.geometry.height,
                    monitor.geometry.x, monitor.geometry.y,
                    monitor.is_primary() ? "*" : ""
                );
            } else {
                info.label = "%d×%d".printf(monitor.geometry.width, monitor.geometry.height);
            }

            var display = Halogen.DisplayManager.get_instance().get_display(this.index);
            display.notify["feed_id"].connect(() => {
                this.set_feed_id(display.feed_id);
            });
            this.set_feed_id(display.feed_id);

            this.feed_chooser.set_row_separator_func((model, iter) => {
                Value val;
                feeds.get_value(iter, 0, out val);
                return val.get_string() == "__separator__";
            });
        }


        private void set_feed_id(string? feed_id) {
            if (feed_id == null) {
                this.feed_chooser.active_id = "__disabled__";
            } else {
                this.feed_chooser.active_id = feed_id;
            }
        }

        [GtkCallback]
        private void on_set_feed() {
            var display = Halogen.DisplayManager.get_instance().get_display(this.index);
            string id = this.feed_chooser.active_id;
            if (id == "__disabled__") display.feed_id = null;
            else display.feed_id = id;
        }
    }
}
