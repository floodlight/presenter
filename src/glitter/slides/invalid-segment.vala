namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/slides/invalid-segment.ui")]
    public class InvalidSegment : Gtk.Box {
        [GtkChild] private unowned Gtk.Label name_label;
        [GtkChild] private unowned Gtk.Stack expand_icon_stack;
        [GtkChild] private unowned Gtk.Revealer revealer;


        private Cellar.Segment segment;


        public InvalidSegment(Cellar.Segment segment) {
            this.segment = segment;
            this.segment.bind_property("name", this.name_label, "label", SYNC_CREATE);
        }


        [GtkCallback]
        private bool on_toggle_expand(Gdk.EventButton event) {
            if (this.revealer.reveal_child) {
                this.revealer.reveal_child = false;
                this.expand_icon_stack.visible_child_name = "collapsed";
            } else {
                this.revealer.reveal_child = true;
                this.expand_icon_stack.visible_child_name = "expanded";
            }

            return false;
        }
    }
}
