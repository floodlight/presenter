namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/slides/slide.ui")]
    public class Slide : Gtk.FlowBoxChild {
        [GtkChild] private unowned Gtk.Label group_label;
        [GtkChild] private unowned Gtk.Box group_box;
        [GtkChild] private unowned Gtk.Image thumbnail;
        [GtkChild] private unowned Gtk.Image continue_automatically;
        [GtkChild] private unowned Gtk.Label continue_delay;
        [GtkChild] private unowned Glitter.SlideEditor slide_editor;
        [GtkChild] private unowned Gtk.Popover edit_popover;


        private Cellar.Segment segment;
        private Cellar.Slide slide;
        private int index;


        public Slide(Cellar.Segment segment, Cellar.Slide slide, int index) {
            this.segment = segment;
            this.slide = slide;
            this.index = index;

            Photon.Config.get_instance().notify["thumbnail-width"].connect(this.rebuild_ui);
            Photon.Config.get_instance().notify["thumbnail-aspect"].connect(this.rebuild_ui);
            slide.changed.connect(this.rebuild_ui);

            this.slide_editor.slide = slide;
            if (this.segment.slideshow.is_internal) {
                this.slide_editor.set_segment_info(segment, index);
            }

            this.rebuild_ui();

            if (slide.group != null) {
                GroupColors.get_instance().bind_label_style(this.group_box, slide.group);
            }
            if (slide.is_first_in_group()) {
                slide.group.bind_property("name", this.group_label, "label", SYNC_CREATE);
            }
        }


        /**
         * Indicates that @resource recently changed and the slide thumbnail
         * should be rerendered if the slide uses that resource.
         */
        public void selectively_rerender(Cellar.Resource resource) {
            if (this.slide.uses_resource(resource) || this.segment.uses_resource(resource)) {
                this.rebuild_ui();
            }
        }


        private void rebuild_ui() {
            this.thumbnail.surface = Thumbnails.get_placeholder();
            Thumbnails.thumbnail_slide(this.thumbnail, segment, slide);

            string continue_tooltip = null;
            switch (this.slide.continue_mode) {
                case MANUAL:
                    this.continue_automatically.visible = false;
                    this.continue_delay.visible = false;
                    break;
                case WHEN_FINISHED:
                    this.continue_automatically.visible = true;
                    this.continue_delay.visible = false;
                    continue_tooltip = _("Slide will advance automatically");
                    break;
                case TIMER:
                    this.continue_automatically.visible = true;
                    this.continue_delay.visible = true;

                    string label = "%.2f".printf(this.slide.continue_delay);
                    this.continue_delay.label = label;
                    continue_tooltip = _("Slide will advance after %s seconds").printf(label);

                    break;
            }
            if (continue_tooltip != null) {
                this.continue_delay.tooltip_text = continue_tooltip;
                this.continue_automatically.tooltip_text = continue_tooltip;
            }
        }


        [GtkCallback]
        private bool on_button_press_event(Gdk.EventButton event) {
            if (event.button == Gdk.BUTTON_SECONDARY) {
                this.edit_popover.popup();
                return true;
            }
            return false;
        }
    }
}
