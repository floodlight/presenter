namespace Glitter.UIState {
    private File get_file() {
        return File.new_for_path(Photon.Config.get_instance().datadir)
               .get_child("uistate.json");
    }

    public void save() {
        Json.Object json = new Json.Object();
        var console = PresenterConsole.get_instance();

        int w, h;
        console.get_size(out w, out h);
        json.set_int_member("window-width", w);
        json.set_int_member("window-height", h);

        json.set_int_member("main-split", console.main_split.position);
        json.set_int_member("left-split", console.left_split.position);

        json.set_boolean_member("library-shown", console.library_revealer.reveal_child);
        json.set_boolean_member("playlist-shown", console.playlist_revealer.reveal_child);

        json.set_object_member("tabs", console.tabs.save_tabs());

        // Photon.save_json() won't work because it's asynchronous, and we
        // need to save the file before the application quits.
        var root = new Json.Node(Json.NodeType.OBJECT);
        root.set_object(json);
        var generator = new Json.Generator();
        generator.pretty = Photon.Config.get_instance().debugmode;
        generator.root = root;

        try {
            generator.to_file(get_file().get_path());
        } catch (Error e) {
            warning("Could not save UI state file! %s", e.message);
        }
    }

    public void restore() {
        Json.Object json;
        try {
            var parser = new Json.Parser();
            parser.load_from_file(get_file().get_path());
            json = parser.get_root().get_object();
        } catch (Error e) {
            return;
        }

        var console = PresenterConsole.get_instance();

        if (json.has_member("window-width") && json.has_member("window-height")) {
            console.resize(
                (int) json.get_int_member("window-width"),
                (int) json.get_int_member("window-height")
            );
        }

        if (json.has_member("main-split")) {
            console.main_split.position = (int) json.get_int_member("main-split");
        }
        if (json.has_member("left-split")) {
            console.left_split.position = (int) json.get_int_member("left-split");
        }

        if (json.has_member("playlist-shown")) {
            console.set_show_playlist(json.get_boolean_member("playlist-shown"));
        }
        if (json.has_member("library-shown")) {
            console.set_show_library(json.get_boolean_member("library-shown"));
        }

        if (json.has_member("tabs")) {
            console.tabs.restore_tabs(json.get_object_member("tabs"));
        }
    }
}
