namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/headerbar/playlist-switcher.ui")]
    public class PlaylistSwitcher : Gtk.MenuButton {
        [GtkChild] private unowned Gtk.ListStore folder_store;
        [GtkChild] private unowned Gtk.TreeSelection folder_selection;
        [GtkChild] private unowned Gtk.TreeView folder_view;
        [GtkChild] private unowned Gtk.CellRendererText folder_name_cell;
        [GtkChild] private unowned Gtk.TreeViewColumn folder_name_column;
        [GtkChild] private unowned Gtk.ListStore playlist_store;
        [GtkChild] private unowned Gtk.TreeModelSort playlist_store_sorted;
        [GtkChild] private unowned Gtk.TreeModelFilter playlist_store_filtered;
        [GtkChild] private unowned MultiDragTreeView playlist_view;
        [GtkChild] private unowned Gtk.TreeSelection playlist_selection;
        [GtkChild] private unowned Gtk.Popover create_playlist_popover;
        [GtkChild] private unowned Gtk.Entry create_playlist_entry;
        [GtkChild] private unowned Gtk.Button create_playlist_button;
        [GtkChild] private unowned Gtk.MenuButton create_playlist_menu_button;
        [GtkChild] private unowned Gtk.Popover create_folder_popover;
        [GtkChild] private unowned Gtk.Entry create_folder_entry;
        [GtkChild] private unowned Gtk.Button create_folder_button;
        [GtkChild] private unowned Gtk.Label open_playlist_label;
        [GtkChild] private unowned Glitter.PlaylistView playlist_preview;
        [GtkChild] private unowned Gtk.CellRendererText playlist_name_cell;
        [GtkChild] private unowned Gtk.TreeViewColumn playlist_name_column;
        [GtkChild] private unowned Gtk.Menu playlist_menu;
        [GtkChild] private unowned Gtk.MenuItem restore_playlist_item;
        [GtkChild] private unowned Gtk.MenuItem trash_playlist_item;
        [GtkChild] private unowned Gtk.Menu folder_menu;
        [GtkChild] private unowned Gtk.Box trash_actions;
        [GtkChild] private unowned Gtk.Popover empty_trash_popover;
        [GtkChild] private unowned Gtk.Button open_button;


        private const string TRASH_FOLDER = "trash";
        private const string ALL_FOLDER = "null";


        private string selected_folder = null;
        private Cellar.PlaylistEntry selected_playlist = null;

        private Gtk.TreePath context_menu_path;


        construct {
            this.playlist_store_filtered.set_visible_func((model, iter) => {
                Value v_folder, v_trash;
                model.get_value(iter, 2, out v_folder);
                string folder = v_folder.get_string();
                model.get_value(iter, 5, out v_trash);
                bool trash = v_trash.get_boolean();

                if (this.selected_folder == TRASH_FOLDER) return trash;
                else if (trash) return false;
                else if (this.selected_folder == null) return true;
                else return folder == this.selected_folder;
            });

            this.folder_view.set_row_separator_func(this.separator_func);

            // sort by last access, descending (latest to earliest)
            this.playlist_store_sorted.set_sort_column_id(4, Gtk.SortType.DESCENDING);

            this.playlist_view.enable_model_drag_source(
                BUTTON1_MASK, { get_drag_target_playlists() }, COPY
            );
            Gtk.drag_dest_set(this.folder_view,
                Gtk.DestDefaults.ALL - Gtk.DestDefaults.HIGHLIGHT,
                { get_drag_target_playlists() }, COPY
            );


            var playlists = Cellar.PlaylistIndex.get_instance();

            foreach (Gee.Map.Entry<string, string> folder in playlists.get_folders().entries) {
                this.on_folder_added(folder.key, folder.value);
            }
            playlists.folder_added.connect(this.on_folder_added);
            playlists.folder_renamed.connect(this.on_folder_renamed);
            playlists.folder_removed.connect(this.on_folder_removed);

            this.select_folder(ALL_FOLDER);

            foreach (Cellar.PlaylistEntry entry in playlists.get_playlists()) {
                this.on_playlist_added(entry);
            }
            this.playlist_selection.unselect_all();
            playlists.playlist_added.connect(this.on_playlist_added);
            playlists.playlist_changed.connect(this.on_playlist_changed);
            playlists.playlist_removed.connect(this.on_playlist_removed);

            playlists.notify["open-playlist"].connect(() => {
                this.open_playlist_label.label = playlists.open_playlist.name;
            });
            this.open_playlist_label.label = playlists.open_playlist.name;
        }


        private bool separator_func(Gtk.TreeModel model, Gtk.TreeIter iter) {
            Value val;
            model.get_value(iter, 0, out val);
            return val.get_string() == "separator";
        }

        private void select_folder(string folder_uuid) {
            this.folder_view.model.@foreach((model, path, iter) => {
                Value v_uuid;
                model.get_value(iter, 0, out v_uuid);
                string uuid = v_uuid.get_string();
                if (folder_uuid == uuid) {
                    this.folder_selection.select_path(path);
                    return true;
                }
                return false;
            });
        }

        private void on_folder_added(string uuid, string name) {
            this.folder_store.insert_with_values(null, -1,
                0, uuid,
                1, name,
                2, true
            );
            this.select_folder(uuid);
        }

        private void on_folder_renamed(string uuid, string new_name) {
            this.folder_store.@foreach((model, path, iter) => {
                Value v_uuid;
                model.get_value(iter, 0, out v_uuid);
                if (v_uuid.get_string() == uuid) {
                    this.folder_store.@set(iter,
                        0, uuid,
                        1, new_name,
                        2, true
                    );
                }
                return false;
            });
        }

        private void on_folder_removed(string uuid) {
            this.folder_store.@foreach((model, path, iter) => {
                Value v_uuid;
                model.get_value(iter, 0, out v_uuid);
                if (v_uuid.get_string() == uuid) {
                    this.folder_store.remove(ref iter);
                    return true;
                }
                return false;
            });
        }

        private void select_playlist(string playlist_uuid) {
            this.playlist_view.model.@foreach((model, path, iter) => {
                Value v_uuid;
                model.get_value(iter, 0, out v_uuid);
                string uuid = v_uuid.get_string();
                if (playlist_uuid == uuid) {
                    this.playlist_selection.unselect_all();
                    this.playlist_selection.select_path(path);
                    return true;
                }
                return false;
            });
        }

        private void on_playlist_added(Cellar.PlaylistEntry entry) {
            this.playlist_store.insert_with_values(null, -1,
                0, entry.uuid,
                1, entry.name,
                2, entry.folder,
                3, new DateTime.from_unix_utc(entry.last_access).format("%x"),
                4, entry.last_access,
                5, entry.trash, -1
            );
            this.select_playlist(entry.uuid);
        }

        private void on_playlist_changed(Cellar.PlaylistEntry entry) {
            this.playlist_store.@foreach((model, path, iter) => {
                Value v_uuid;
                model.get_value(iter, 0, out v_uuid);
                string uuid = v_uuid.get_string();
                if (uuid == entry.uuid) {
                    this.playlist_store.@set(iter,
                        0, entry.uuid,
                        1, entry.name,
                        2, entry.folder,
                        3, new DateTime.from_unix_utc(entry.last_access).format("%x"),
                        4, entry.last_access,
                        5, entry.trash, -1
                    );
                }

                return false;
            });

            var playlists = Cellar.PlaylistIndex.get_instance();
            if (entry == playlists.open_playlist) {
                this.open_playlist_label.label = entry.name;
            }
        }

        private void on_playlist_removed(Cellar.PlaylistEntry entry) {
            this.playlist_store.@foreach((model, path, iter) => {
                Value v_uuid;
                model.get_value(iter, 0, out v_uuid);
                if (v_uuid.get_string() == entry.uuid) {
                    this.playlist_store.remove(ref iter);
                    return true;
                }
                return false;
            });
        }


        [GtkCallback]
        private void on_playlist_selection_changed() {
            Gtk.TreeModel model;
            List<Gtk.TreePath> rows = this.playlist_selection.get_selected_rows(out model);
            if (rows.length() == 1) {
                Gtk.TreeIter iter;
                model.get_iter(out iter, rows.nth_data(0));

                Value val;
                model.get_value(iter, 0, out val);
                string uuid = val.get_string();

                if (uuid == null) return;

                this.selected_playlist = Cellar.PlaylistIndex.get_instance()
                                         .get_playlist_entry(uuid);

                this.playlist_preview.playlist = this.selected_playlist.get_playlist();
                this.open_button.sensitive = true;
            } else {
                this.playlist_preview.playlist = null;
                this.open_button.sensitive = false;
            }
        }


        private void open_playlist() {
            if (this.selected_playlist != null) {
                Cellar.Playlist playlist = this.selected_playlist.get_playlist();
                Halogen.State.get_instance().playlist = playlist;
                this.popover.popdown();
            }
        }
        [GtkCallback]
        private void on_open_playlist_row(Gtk.TreeView widget,
                                          Gtk.TreePath path,
                                          Gtk.TreeViewColumn col) {
            this.open_playlist();
        }
        [GtkCallback]
        private void on_open_playlist_clicked(Gtk.Button widget) {
            this.open_playlist();
        }


        [GtkCallback]
        private void on_create_playlist_menu_button_clicked() {
            if (this.create_playlist_menu_button.active) {
                this.create_playlist_entry.text = new DateTime.now().format("%a. %B %e");
                focus_and_select(this.create_playlist_entry);
            }
        }

        [GtkCallback]
        private void on_create_playlist_clicked(Gtk.Widget widget) {
            var name = this.create_playlist_entry.text;
            var playlist = new Cellar.PlaylistEntry.from_scratch(name);
            playlist.folder = this.selected_folder;

            var playlists = Cellar.PlaylistIndex.get_instance();
            playlists.add_playlist(playlist);
            this.create_playlist_entry.text = "";
            this.create_playlist_popover.popdown();
        }

        [GtkCallback]
        private void on_playlist_name_entry_changed() {
            string text = this.create_playlist_entry.text;
            this.create_playlist_button.sensitive = text.strip() != "";
        }

        [GtkCallback]
        private void on_create_folder_clicked(Gtk.Widget widget) {
            var playlists = Cellar.PlaylistIndex.get_instance();
            playlists.add_folder(this.create_folder_entry.text);
            this.create_folder_entry.text = "";
            this.create_folder_popover.popdown();
        }

        [GtkCallback]
        private void on_folder_name_entry_changed() {
            var playlists = Cellar.PlaylistIndex.get_instance();
            string text = this.create_folder_entry.text;
            this.create_folder_button.sensitive = playlists.is_valid_folder_name(text);
        }

        [GtkCallback]
        private void on_folder_selection_changed() {
            Gtk.TreeModel model;
            List<Gtk.TreePath> rows = this.folder_selection.get_selected_rows(out model);
            if (rows.length() == 1) {
                Gtk.TreeIter iter;
                model.get_iter(out iter, rows.nth_data(0));

                Value val;
                model.get_value(iter, 0, out val);
                string uuid = val.get_string();
                if (uuid == ALL_FOLDER) uuid = null;

                this.selected_folder = uuid;

                if (uuid == TRASH_FOLDER) {
                    this.trash_actions.show();
                    this.create_playlist_menu_button.sensitive = false;
                } else {
                    this.trash_actions.hide();
                    this.create_playlist_menu_button.sensitive = true;
                }

                this.playlist_store_filtered.refilter();
            }
        }

        [GtkCallback]
        private void on_folder_name_edited(string path_str, string new_text) {
            var path = new Gtk.TreePath.from_string(path_str);
            Gtk.TreeIter iter;
            Value v_uuid;

            this.folder_view.model.get_iter(out iter, path);
            this.folder_view.model.get_value(iter, 0, out v_uuid);

            Cellar.PlaylistIndex.get_instance().rename_folder(v_uuid.get_string(), new_text);
        }

        private Cellar.PlaylistEntry get_playlist_at_path(Gtk.TreePath path) {
            Gtk.TreeIter iter;
            this.playlist_view.model.get_iter(out iter, path);
            Value v_uuid;
            this.playlist_view.model.get_value(iter, 0, out v_uuid);

            return Cellar.PlaylistIndex.get_instance().get_playlist_entry(v_uuid.get_string());
        }

        [GtkCallback]
        private void on_playlist_right_clicked(Gtk.TreePath[] rows, Gdk.EventButton event) {
            if (rows.length == 1) {
                var path = rows[0];
                this.context_menu_path = path;
                Cellar.PlaylistEntry entry = this.get_playlist_at_path(path);

                if (entry.trash) {
                    this.restore_playlist_item.show();
                    this.trash_playlist_item.hide();
                } else {
                    this.restore_playlist_item.hide();
                    this.trash_playlist_item.show();
                }

                this.playlist_menu.popup_at_pointer(event);
            }
        }

        [GtkCallback]
        private void on_rename_playlist_item_activated() {
            // make the name cell editable temporarily. this is normally
            // disabled so that double clicking activates the row instead of
            // editing
            this.playlist_name_cell.editable = true;
            this.playlist_view.set_cursor_on_cell(
                this.context_menu_path, this.playlist_name_column,
                this.playlist_name_cell, true
            );
        }

        [GtkCallback]
        private void on_playlist_name_edited(string path_str, string new_text) {
            if (!Cellar.PlaylistEntry.is_valid_playlist_name(new_text)) return;

            var path = new Gtk.TreePath.from_string(path_str);
            Gtk.TreeIter iter;
            Value v_uuid;

            this.playlist_view.model.get_iter(out iter, path);
            this.playlist_view.model.get_value(iter, 0, out v_uuid);

            Cellar.PlaylistEntry entry = Cellar.PlaylistIndex.get_instance()
                                         .get_playlist_entry(v_uuid.get_string());

            entry.name = new_text;

            // make cell uneditable again so that double clicking opens the
            // playlist
            this.playlist_name_cell.editable = false;
        }

        [GtkCallback]
        private void on_playlist_trash_item_activated() {
            this.get_playlist_at_path(this.context_menu_path).trash = true;
        }

        [GtkCallback]
        private void on_playlist_restore_item_activated() {
            this.get_playlist_at_path(this.context_menu_path).trash = false;
        }

        [GtkCallback]
        private void on_confirm_empty_trash_clicked() {
            Cellar.PlaylistIndex.get_instance().empty_trash();
            this.empty_trash_popover.popdown();
            this.select_folder("null");
        }

        private string get_folder_at_path(Gtk.TreePath path) {
            Gtk.TreeIter iter;
            this.folder_view.model.get_iter(out iter, path);
            Value v_uuid;
            this.folder_view.model.get_value(iter, 0, out v_uuid);
            return v_uuid.get_string();
        }

        [GtkCallback]
        private bool on_folder_clicked(Gdk.EventButton event) {
            if (event.button == Gdk.BUTTON_SECONDARY) {
                Gtk.TreePath path;
                this.folder_view.get_path_at_pos((int) event.x, (int) event.y, out path, null, null, null);
                this.context_menu_path = path;

                if (path != null) {
                    string folder = this.get_folder_at_path(path);
                    if (folder != null && folder != ALL_FOLDER && folder != TRASH_FOLDER) {
                        this.folder_menu.popup_at_pointer(event);
                    }
                }
            }

            return false;
        }

        [GtkCallback]
        private void on_folder_trash_item_activated() {
            string id = this.get_folder_at_path(this.context_menu_path);
            Cellar.PlaylistIndex.get_instance().remove_folder(id);
            this.select_folder(TRASH_FOLDER);
        }

        [GtkCallback]
        private void on_rename_folder_item_activated() {
            this.folder_view.set_cursor_on_cell(
                this.context_menu_path, this.folder_name_column,
                this.folder_name_cell, true
            );
        }

        [GtkCallback]
        private bool on_folder_drag_motion(Gdk.DragContext ctx, int x, int y, uint time) {
            Gtk.TreePath path;
            Gtk.TreeIter iter;
            this.folder_view.get_path_at_pos(x, y, out path, null, null, null);
            if (path == null) {
                this.folder_view.set_drag_dest_row(null, INTO_OR_AFTER);
                return false;
            }

            this.folder_store.get_iter(out iter, path);

            if (this.separator_func(this.folder_store, iter)) {
                this.folder_view.set_drag_dest_row(null, INTO_OR_AFTER);
                return false;
            }

            Gdk.drag_status(ctx, COPY, time);
            this.folder_view.set_drag_dest_row(path, INTO_OR_AFTER);
            return true;
        }

        [GtkCallback]
        private void on_folder_drag_data_received(Gdk.DragContext ctx,
                                                  int x, int y,
                                                  Gtk.SelectionData selection,
                                                  uint info,
                                                  uint time) {
            string[] playlists = ((string) selection.get_data()).split(";");
            var index = Cellar.PlaylistIndex.get_instance();

            Gtk.TreePath path;
            Gtk.TreeIter iter;
            Value v_id;

            this.folder_view.get_path_at_pos(x, y, out path, null, null, null);
            this.folder_view.model.get_iter(out iter, path);
            this.folder_view.model.get_value(iter, 0, out v_id);
            string dest_folder = v_id.get_string();

            foreach (var playlist_id in playlists) {
                Cellar.PlaylistEntry entry = index.get_playlist_entry(playlist_id);
                if (dest_folder == TRASH_FOLDER) {
                    entry.trash = true;
                } else {
                    entry.trash = false;
                    if (dest_folder == ALL_FOLDER) entry.folder = null;
                    else entry.folder = dest_folder;
                }
            }

            this.select_folder(dest_folder);
        }

        [GtkCallback]
        private void on_playlist_drag_data_get(Gdk.DragContext context,
                                               Gtk.SelectionData selection,
                                               uint info,
                                               uint time) {
            Gtk.TreeModel model;
            List<Gtk.TreePath> rows = this.playlist_view.get_selection()
                                      .get_selected_rows(out model);

            string[] ids = {};
            foreach (var row in rows) {
                Gtk.TreeIter iter;
                Value v_id;
                model.get_iter(out iter, row);
                model.get_value(iter, 0, out v_id);
                ids += v_id.get_string();
            }

            Gtk.TargetEntry target = get_drag_target_playlists();
            selection.@set(
                Gdk.Atom.intern(target.target, false),
                0,
                string.joinv(";", ids).data
            );
        }
    }
}
