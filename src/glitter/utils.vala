namespace Glitter {
    public Gtk.Clipboard clipboard() {
        return Gtk.Clipboard.get_default(Gdk.Display.get_default());
    }

    public void clear_widget(Gtk.Container container) {
        container.foreach(child => container.remove(child));
    }


    /**
     * Focuses a text entry and selects all text in it. Helpful for when rename
     * entries pop up.
     */
    public void focus_and_select(Gtk.Entry entry) {
        entry.select_region(0, -1);
        entry.grab_focus();
    }


    public void flowbox_remove_highlight(Gtk.FlowBox flowbox) {
        foreach (Gtk.Widget child in flowbox.get_children()) {
            Gtk.StyleContext ctx = child.get_style_context();
            ctx.remove_class("drag-hover-end");
            ctx.remove_class("drag-hover-start");
        }
    }

    /**
     * Visually indicates that something will be dropped between two rows of a
     * listbox.
     */
    public void flowbox_set_highlight(Gtk.FlowBox flowbox, int x, int y) {
        flowbox_remove_highlight(flowbox);
        if (y == -1) return;

        Gtk.FlowBoxChild child = flowbox.get_child_at_pos(x, y);
        int last = (int) flowbox.get_children().length() - 1;

        Gtk.FlowBoxChild child_prev = null;
        Gtk.FlowBoxChild child_next = null;
        if (child == null) {
            child_prev = flowbox.get_child_at_index(last);
        } else {
            Gtk.Allocation alloc;
            child.get_allocation(out alloc);

            if (x < alloc.x + alloc.width / 2) {
                child_next = child;
                if (child.get_index() > 0) {
                    child_prev = flowbox.get_child_at_index(child.get_index() - 1);
                }
            } else {
                child_prev = child;
                if (child.get_index() < last) {
                    child_next = flowbox.get_child_at_index(child.get_index() + 1);
                }
            }
        }

        if (child_prev != null) {
            child_prev.get_style_context().add_class("drag-hover-end");
        }
        if (child_next != null) {
            child_next.get_style_context().add_class("drag-hover-start");
        }
    }

    public void listbox_remove_highlight(Gtk.ListBox listbox) {
        foreach (Gtk.Widget row in listbox.get_children()) {
            Gtk.StyleContext ctx = row.get_style_context();
            ctx.remove_class("drag-hover-bottom");
            ctx.remove_class("drag-hover-top");
        }
    }

    /**
     * Visually indicates that something will be dropped between two rows of a
     * listbox.
     *
     * If `y` is -1, any existing drop highlight will be removed.
     */
    public void listbox_set_highlight(Gtk.ListBox listbox, int y=-1) {
        listbox_remove_highlight(listbox);
        if (y == -1) return;

        Gtk.ListBoxRow row = listbox.get_row_at_y(y);
        int last = (int) listbox.get_children().length() - 1;

        Gtk.ListBoxRow row_prev = null;
        Gtk.ListBoxRow row_next = null;
        if (row == null) {
            row_prev = listbox.get_row_at_index(last);
        } else {
            Gtk.Allocation alloc;
            row.get_allocation(out alloc);

            if (y < alloc.y + alloc.height / 2) {
                row_next = row;
                if (row.get_index() > 0) {
                    row_prev = listbox.get_row_at_index(row.get_index() - 1);
                }
            } else {
                row_prev = row;
                if (row.get_index() < last) {
                    row_next = listbox.get_row_at_index(row.get_index() + 1);
                }
            }
        }

        if (row_prev != null) {
            row_prev.get_style_context().add_class("drag-hover-bottom");
        }
        if (row_next != null) {
            row_next.get_style_context().add_class("drag-hover-top");
        }
    }

    /**
     * Tree view that allows multiple rows to be dragged.
     *
     * Adapted from https://kevinmehall.net/2010/pygtk_multi_select_drag_drop,
     * MIT licensed
     */
    public class MultiDragTreeView : Gtk.TreeView {
        public signal void right_clicked(Gtk.TreePath[] rows, Gdk.EventButton event);


        private Gtk.TreePath? defer_select = null;


        construct {
            this.button_press_event.connect(this.on_button_press);
            this.button_release_event.connect(this.on_button_release);
            this.drag_begin.connect_after(this.on_drag_begin);
            this.drag_end.connect(this.on_drag_end);
        }


        private bool on_button_press(Gdk.EventButton event) {
            var selection = this.get_selection();
            Gtk.TreePath target;
            this.get_path_at_pos((int) event.x, (int) event.y, out target, null, null, null);

            if (event.button == Gdk.BUTTON_SECONDARY) {
                if (target == null) {
                    selection.unselect_all();
                } else if (!selection.path_is_selected(target)) {
                    selection.unselect_all();
                    selection.select_path(target);
                }

                Gtk.TreePath[] selected = {};
                selection.selected_foreach((model, path, iter) => {
                    selected += path;
                });
                this.right_clicked(selected, event);

                // don't select stuff on right click
                return true;
            }

            if (target != null
                && event.type == BUTTON_PRESS
                && !(Gdk.ModifierType.CONTROL_MASK in event.state)
                && !(Gdk.ModifierType.SHIFT_MASK in event.state)
                && selection.path_is_selected(target)) {

                // disable selection
                selection.set_select_function(() => false);
                this.defer_select = target;
            }

            return false;
        }

        private bool on_button_release(Gdk.EventButton event) {
            // selection only works on left click
            if (event.button != Gdk.BUTTON_PRIMARY) return true;

            // re-enable selection
            this.get_selection().set_select_function(() => true);

            Gtk.TreePath target;
            Gtk.TreeViewColumn column;
            this.get_path_at_pos((int) event.x, (int) event.y, out target, out column, null, null);
            if (target != null
                && this.defer_select != null
                && this.defer_select.compare(target) == 0
                && !(event.x == 0 && event.y == 0)) {

                this.set_cursor(target, column, false);
            }
            this.defer_select = null;

            return false;
        }

        private void on_drag_begin(Gdk.DragContext context) {
            Cairo.Surface[] surfaces = {};
            int width = 0;
            int height = 0;
            int[] heights = {};

            foreach (Gtk.TreeViewColumn col in this.get_columns()) {
                width += col.width;
            }

            int count = 0;
            foreach (Gtk.TreePath row in this.get_selection().get_selected_rows(null)) {
                Gdk.Rectangle rect;
                this.get_cell_area(row, null, out rect);
                height += rect.height + 1;
                heights += rect.height;

                surfaces += this.create_row_drag_icon(row);

                count ++;
            }

            var surf = new Cairo.ImageSurface(ARGB32, width + 2, height + 3);
            var ctx = new Cairo.Context(surf);
            ctx.set_source_rgb(0, 0, 0);
            ctx.paint();

            int y = 2;
            for (int i = 0; i < surfaces.length; i ++) {
                ctx.set_source_surface(surfaces[i], 2, y);
                ctx.paint();
                y += heights[i] + 1;
            }

            Gtk.drag_set_icon_surface(context, surf);
        }

        private void on_drag_end(Gdk.DragContext context) {
            // re-enable selection
            this.get_selection().set_select_function(() => true);
        }
    }
}
