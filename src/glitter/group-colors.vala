namespace Glitter {
    public class GroupColors {
        public const string CSS_PREFIX = "group-color-";

        private static GroupColors instance;
        public static GroupColors get_instance() {
            if (instance == null) instance = new GroupColors();
            return instance;
        }


        private Gee.HashMap<string, string> colors;


        private GroupColors() {
            colors = new Gee.HashMap<string, string>();

            colors["default"] = "#7f7f7f";

            colors["verse"]  = "#7f3f00";
            colors["verse1"] = colors["verse"];
            colors["verse2"] = "#007f3f";
            colors["verse3"] = "#7f003f";
            colors["verse4"] = "#003f7f";
            colors["verse5"] = "#3f7f00";
            colors["verse6"] = "#3f007f";

            colors["chorus"]  = "#ff0000";
            colors["chorus1"] = colors["chorus"];
            colors["chorus2"] = "#00ff00";
            colors["chorus3"] = "#0033ff";

            colors["prechorus"]  = "#ff6666";
            colors["prechorus1"] = colors["prechorus"];
            colors["prechorus2"] = "#33ff33";
            colors["prechorus3"] = "#6666ff";

            colors["tag"] = "#5f5f5f";
            colors["bridge"] = "#9f9f9f";
        }


        public string get_css() {
            string res = "";

            foreach (var entry in this.colors.entries) {
                res += @".$(CSS_PREFIX)$(entry.key){background-color:$(entry.value)}";
            }

            return res;
        }

        public string get_class_name_from_string(string? str) {
            string name = str.strip().casefold().replace(" ", "");
            if (this.colors.has_key(name)) {
                return CSS_PREFIX + name;
            } else {
                return CSS_PREFIX + "default";
            }
        }

        /**
         * Gets the CSS class name for the given group, or the default class
         * name if @group is null.
         */
        public string get_class_name(Cellar.Group? group) {
            if (group == null || group.name == null) {
                return CSS_PREFIX + "default";
            }

            return get_class_name_from_string(group.name);
        }

        /**
         * Sets the widget's background color to represent the given group, using
         * CSS styling. Removes group styling if the group name is null.
         */
        public void set_label_style(Gtk.Widget widget, Cellar.Group? group) {
            var style = widget.get_style_context();
            foreach (string c in style.list_classes()) {
                if (c.has_prefix(CSS_PREFIX)) style.remove_class(c);
            }

            style.add_class(this.get_class_name(group));

        }

        public void set_label_style_from_string(Gtk.Widget widget, string? name) {
            var style = widget.get_style_context();
            foreach (string c in style.list_classes()) {
                if (c.has_prefix(CSS_PREFIX)) style.remove_class(c);
            }

            style.add_class(this.get_class_name_from_string(name));
        }

        /**
         * Same as set_group_label_style but keeps the widget style updated when
         * the group name changes.
         */
        public void bind_label_style(Gtk.Widget widget, Cellar.Group group) {
            var id = group.notify["name"].connect(() => this.set_label_style(widget, group));
            widget.destroy.connect(() => group.disconnect(id));
            set_label_style(widget, group);
        }
    }
}
