namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/common/library-view.ui")]
    public class LibraryView : Gtk.Box {
        /**
         * Emitted when the user activates a row in the library list view.
         */
        public signal void resource_activated(Cellar.Resource resource);


        /**
         * Whether the library view is the main one in the interface. Only that
         * library view allows editing the library; the rest are only for
         * selecting a resource.
         */
        public bool editor { get; construct; }

        private bool _media_only = false;
        /**
         * If true, only media resources will be shown in the list.
         */
        public bool media_only {
            get {
                return _media_only;
            }
            set {
                _media_only = value;
                this.refilter();
            }
        }


        [GtkChild] private unowned Gtk.SearchEntry search_entry;
        [GtkChild] private unowned MultiDragTreeView list_view;
        [GtkChild] private unowned Gtk.Popover preview;
        [GtkChild] private unowned Gtk.Image preview_thumbnail;
        [GtkChild] private unowned Gtk.Menu context_menu;
        [GtkChild] private unowned Gtk.MenuItem separator_1;
        [GtkChild] private unowned Gtk.MenuItem edit_item;
        [GtkChild] private unowned Gtk.MenuItem rename_item;
        [GtkChild] private unowned Gtk.MenuItem delete_item;
        [GtkChild] private unowned Gtk.MenuItem add_paste_lyrics_item;
        [GtkChild] private unowned Gtk.Popover import_popover;
        [GtkChild] private unowned Gtk.FileChooserWidget file_chooser;
        [GtkChild] private unowned Gtk.Button import_button;
        [GtkChild] private unowned Gtk.ScrolledWindow scroll_window;
        [GtkChild] private unowned Gtk.CellRendererText resource_name_cell;
        [GtkChild] private unowned Gtk.TreeViewColumn tree_column;
        [GtkChild] private unowned Gtk.Popover import_options;
        [GtkChild] private unowned Gtk.Popover delete_popover;
        [GtkChild] private unowned Gtk.Label delete_label;


        Gtk.TreeModelFilter filtered_store;
        /**
         * This aptly named property exists to work around a circular reference
         * that would exist if filter_func were part of LibraryView.
         *
         * Setting a TreeModelFilter's visible_func references the callback's
         * user_data, which would be the LibraryView.
         *
         * So, the workaround changes the references to this:
         *
         *     LibraryView -> LibraryViewFilterer
         *                 -> TreeModelFilter ^
         *
         * From this:
         *
         *     LibraryView <-> TreeModelFilter
         */
        LibraryViewFilterer hacky_workaround;
        Gtk.TreeModelSort sorted_store;


        private Gtk.TreePath[] context_menu_paths;


        /**
         * The last resource in the last that was shown in the preview popover.
         */
        private Cellar.Resource? last_preview;

        /**
         * The URIs that need to be imported. This is stored while additional
         * import options are chosen, if necessary.
         */
        private string[] import_uris;


        construct {
            this.filtered_store = new Gtk.TreeModelFilter(LibraryStore.get_instance(), null);
            this.hacky_workaround = new LibraryViewFilterer();
            this.filtered_store.set_visible_func(this.hacky_workaround.filter_func);

            this.sorted_store = new Gtk.TreeModelSort.with_model(this.filtered_store);
            this.sorted_store.set_sort_column_id(4, Gtk.SortType.DESCENDING);

            this.list_view.model = this.sorted_store;
            this.list_view.model.row_inserted.connect(this.on_row_inserted);

            var cell_render = new ColoredIconRenderer();
            this.tree_column.pack_start(cell_render, false);
            this.tree_column.set_attributes(cell_render,
                "resource-type", 2,
                "icon-name", 0
            );
            this.tree_column.reorder(cell_render, 0);

            // Set up drag-and-drop
            if (this.editor) {
                Gtk.drag_dest_set(this.scroll_window, Gtk.DestDefaults.ALL, {}, Gdk.DragAction.COPY);
                Gtk.drag_dest_add_uri_targets(this.scroll_window);

                // Do not allow drag and drop in the search entry, as that
                // might be confusing
                Gtk.drag_dest_unset(this.search_entry);

                // For dragging resources into the playlist view
                this.list_view.enable_model_drag_source(
                    Gdk.ModifierType.BUTTON1_MASK,
                    { get_drag_target_resources() },
                    Gdk.DragAction.COPY
                );
            }

            // Filtering by MIME type wildcard does not work on Windows, so
            // just show all files for now
            if (Photon.OS == Photon.WINDOWS) {
                this.file_chooser.filter.add_pattern("*");
            }

            // for now, only show expanders in debug mode
            this.list_view.show_expanders = Photon.Config.get_instance().debugmode;
        }


        private Cellar.Resource get_resource_for_path(Gtk.TreePath path) {
            Gtk.TreeIter iter;
            this.list_view.model.get_iter(out iter, path);
            Value v_id;
            this.list_view.model.get_value(iter, 0, out v_id);
            return Cellar.Library.get_instance().get_resource(v_id.get_string());
        }

        private void refilter() {
            this.hacky_workaround.search_term = this.search_entry.text;
            this.hacky_workaround.media_only = this.media_only;
            this.filtered_store.refilter();
        }

        [GtkCallback]
        private void on_row_activated(Gtk.TreePath path, Gtk.TreeViewColumn column) {
            Gtk.TreeIter iter;
            this.list_view.model.get_iter(out iter, path);
            Value v_id;
            this.list_view.model.get_value(iter, 0, out v_id);

            Cellar.Resource res = Cellar.Library.get_instance().get_resource(v_id.get_string());
            this.resource_activated(res);
        }

        [GtkCallback]
        private void on_search_changed() {
            this.refilter();
        }

        [GtkCallback]
        private bool on_mouse_over(Gdk.EventMotion event) {
            Gtk.TreePath path;
            this.list_view.get_path_at_pos((int) event.x, (int) event.y, out path, null, null, null);

            if (path == null) {
                this.preview.popdown();
                this.last_preview = null;
            } else {
                Cellar.Resource res = this.get_resource_for_path(path);

                if (this.last_preview == res) return false;
                this.last_preview = res;

                if (res.thumbnail != null) {
                    Thumbnails.thumbnail_resource(this.preview_thumbnail, res, 0, 0);

                    Gdk.Rectangle rect;
                    this.list_view.get_cell_area(path, null, out rect);
                    rect.width = this.list_view.get_allocated_width();
                    this.preview.pointing_to = rect;
                    this.preview.popup();
                } else {
                    this.preview.popdown();
                }
            }

            return false;
        }

        [GtkCallback]
        private bool on_mouse_leave(Gdk.EventCrossing event) {
            this.preview.popdown();
            this.last_preview = null;
            return false;
        }

        [GtkCallback]
        private void on_listview_right_clicked(Gtk.TreePath[] rows, Gdk.EventButton event) {
            if (!this.editor) return;

            this.context_menu_paths = rows;

            if (rows.length == 1) {
                this.context_menu.show_all();

                // Hide "Paste Lyrics" if the clipboard does not contain text
                this.add_paste_lyrics_item.visible = clipboard().wait_is_text_available();
            } else {
                this.separator_1.hide();
                this.edit_item.hide();
                this.rename_item.hide();
                this.delete_item.visible = (rows.length > 0);
            }

            this.context_menu.popup_at_pointer(event);
        }

        [GtkCallback]
        private void on_edit_item_activated() {
            if (this.context_menu_paths.length != 1) return;

            Cellar.Resource res = this.get_resource_for_path(this.context_menu_paths[0]);
            var info = new TabInfo.for_resource_editor(res);
            PresenterConsole.get_instance().tabs.show_tab(info);
        }

        [GtkCallback]
        private void on_delete_item_activated() {
            if (this.context_menu_paths.length == 1) {
                Cellar.Resource res = this.get_resource_for_path(this.context_menu_paths[0]);
                this.delete_label.label = _("Are you sure you want to <b>permanently</b> delete %s?")
                    .printf(res.name);
            } else {
                this.delete_label.label = _("Are you sure you want to <b>permanently</b> delete %d items?")
                    .printf(this.context_menu_paths.length);
            }

            this.delete_popover.popup();
        }

        [GtkCallback]
        private void on_delete_confirm_clicked() {
            /* Get a list of resources first, then delete them. Otherwise,
             * the model will be changed as resources are deleted, and the
             * wrong resources will get deleted! */
            string[] ids = {};
            foreach (Gtk.TreePath path in this.context_menu_paths) {
                ids += this.get_resource_for_path(path).get_full_id();
            }
            foreach (string id in ids) {
                Cellar.Library.get_instance().remove_resource(id);
            }

            this.delete_popover.popdown();
        }

        [GtkCallback]
        private void on_delete_cancel_clicked() {
            this.delete_popover.popdown();
        }

        private void add_resource(Cellar.Resource res) {
            Cellar.Library.get_instance().user_shelf.add_resource(res);
        }

        [GtkCallback]
        private void on_add_slideshow_item_activated() {
            var res = new Cellar.Resource.from_scratch(
                Cellar.ResourceType.SLIDESHOW,
                _("New Slideshow"),
                null
            );
            add_resource(res);
        }

        [GtkCallback]
        private void on_add_song_item_activated() {
            var res = new Cellar.Resource.from_scratch(
                Cellar.ResourceType.SONG,
                _("New Song"),
                null
            );
            add_resource(res);
        }

        [GtkCallback]
        private void on_add_paste_lyrics_item_activated() {
            string? lyrics = clipboard().wait_for_text();
            if (lyrics == null) return;

            Cellar.Resource resource = new Cellar.Resource.from_scratch(
                Cellar.ResourceType.SONG,
                _("Song from Clipboard"),
                ""
            );
            Cellar.Slideshow slideshow = Cellar.ImportUtils.parse_lyrics(lyrics, resource);
            slideshow.add_to_library_with_resource(resource);
        }

        [GtkCallback]
        private void on_add_import_item_activated() {
            this.import_popover.popup();
        }

        [GtkCallback]
        private void on_import_cancel_clicked() {
            this.import_popover.popdown();
        }

        /**
         * Initiates a file import with the given files, supplied either by
         * drag-n-drop or the file chooser.
         */
        private void import_files(string[] uris) {
            if (this.import_uris != null) {
                warning("An import is already in progress by this library view!");
                return;
            }

            bool slideshow = Cellar.ImportUtils.can_import_as_slideshow(uris);
            if (slideshow) {
                this.import_uris = uris;
                this.import_options.popup();
            } else {
                var library = Cellar.Library.get_instance();
                library.user_shelf.import_files(uris, false);
            }
        }

        /**
         * Gets the files chosen in the import file chooser and calls
         * import_files(). Then, pops down the file chooser popover.
         */
        private void get_file_chooser_list() {
            string[] uris = {};
            foreach (string uri in this.file_chooser.get_uris()) {
                uris += uri;
            }
            this.import_files(uris);

            this.import_popover.popdown();
        }

        [GtkCallback]
        private void on_import_clicked() {
            this.get_file_chooser_list();
        }

        [GtkCallback]
        private void on_file_activated() {
            this.get_file_chooser_list();
        }

        [GtkCallback]
        private void on_file_selection_changed() {
            this.import_button.sensitive = this.file_chooser.get_uris().length() > 0;
        }

        [GtkCallback]
        private void on_import_as_slideshow_clicked() {
            var library = Cellar.Library.get_instance();
            library.user_shelf.import_files(this.import_uris, true);
            this.import_options.popdown();
            this.import_uris = null;
        }

        [GtkCallback]
        private void on_import_separately_clicked() {
            var library = Cellar.Library.get_instance();
            library.user_shelf.import_files(this.import_uris, false);
            this.import_options.popdown();
            this.import_uris = null;
        }

        [GtkCallback]
        private void on_import_options_closed() {
            this.import_uris = null;
        }

        [GtkCallback]
        private void on_drag_data_received(Gdk.DragContext ctx,
                                           int x, int y,
                                           Gtk.SelectionData selection,
                                           uint info,
                                           uint time) {

            string[] uris = selection.get_uris();
            if (uris != null && uris.length > 0) {
                this.import_files(uris);
            }
        }

        [GtkCallback]
        private void on_drag_data_get(Gdk.DragContext context,
                                      Gtk.SelectionData selection,
                                      uint info,
                                      uint time) {
            Gtk.TreeModel model;
            List<Gtk.TreePath> rows = this.list_view.get_selection().get_selected_rows(out model);

            string[] ids = {};
            foreach (var row in rows) {
                Gtk.TreeIter iter;
                Value v_id;
                model.get_iter(out iter, row);
                model.get_value(iter, 0, out v_id);
                ids += v_id.get_string();
            }

            Gtk.TargetEntry target = get_drag_target_resources();
            selection.@set(
                Gdk.Atom.intern(target.target, false),
                0,
                string.joinv(";", ids).data
            );
        }

        [GtkCallback]
        private void on_rename_item_activated() {
            if (this.context_menu_paths.length != 1) return;

            this.resource_name_cell.editable = true;
            this.list_view.set_cursor_on_cell(
                this.context_menu_paths[0], this.tree_column,
                this.resource_name_cell, true
            );
        }

        [GtkCallback]
        private void on_resource_name_edited(string path_str, string new_text) {
            var path = new Gtk.TreePath.from_string(path_str);
            Gtk.TreeIter iter;
            Value v_uuid;

            this.list_view.model.get_iter(out iter, path);
            this.list_view.model.get_value(iter, 0, out v_uuid);

            string uuid = v_uuid.get_string();
            Cellar.Resource res = Cellar.Library.get_instance().get_resource(uuid);
            res.name = new_text;

            this.resource_name_cell.editable = false;
        }

        /**
         * Signal handler for when a row is added to the list. Scrolls the new
         * row into view so the user is aware of the new resource.
         */
        private void on_row_inserted(Gtk.TreePath path, Gtk.TreeIter iter) {
            this.list_view.scroll_to_cell(path, null, false, 0, 0);
        }
    }


    private class LibraryViewFilterer {
        public string search_term = "";
        public bool media_only = false;

        public bool filter_func(Gtk.TreeModel model, Gtk.TreeIter iter) {
            Value v_name, v_type;
            model.get_value(iter, 1, out v_name);
            model.get_value(iter, 2, out v_type);

            if (this.media_only) {
                var type = Cellar.ResourceType.for_id(v_type.get_string());
                if (!type.is_media()) {
                    return false;
                }
            }

            if (search_term == "") return true;

            return search_term.casefold() in v_name.get_string().casefold();
        }
    }


    private class ColoredIconRenderer : Gtk.CellRendererPixbuf {
        public string resource_type { get; set; }


        public override void render(Cairo.Context cr,
                                    Gtk.Widget widget,
                                    Gdk.Rectangle background,
                                    Gdk.Rectangle cell,
                                    Gtk.CellRendererState flags) {

            Cellar.ResourceType type = Cellar.ResourceType.for_id(this.resource_type);
            this.icon_name = type.icon;

            Gtk.StyleContext style = widget.get_style_context();
            style.save();
            style.add_class("resource-icon-" + type.id);

            base.render(cr, widget, background, cell, flags);

            style.restore();
        }
    }
}
