namespace Glitter {
    [GtkTemplate (ui="/io/gitlab/floodlight/Presenter/ui/common/resource-chooser.ui")]
    public class ResourceChooser : Gtk.Box {
        /**
         * Emitted when the user has chosen a resource (or cleared the
         * choice).
         */
        public signal void resource_chosen(Cellar.Resource? resource);


        private Cellar.Resource? _resource;
        /**
         * The chosen resource, or null if no resource has been chosen.
         */
        public Cellar.Resource? resource {
            get {
                return _resource;
            }
            set {
                if (_resource != null) {
                    _resource.changed.disconnect(this.rebuild_ui);
                    _resource.removed.disconnect(this.rebuild_ui);
                }

                _resource = value;

                if (_resource != null) {
                    _resource.changed.connect(this.rebuild_ui);
                    _resource.removed.connect(this.rebuild_ui);
                }
                this.rebuild_ui();
            }
        }

        /**
         * If true, only media resources can be chosen.
         */
        public bool media_only { get; set; default=true; }


        [GtkChild] private unowned Gtk.Label name_label;
        [GtkChild] private unowned Gtk.Image thumbnail;
        [GtkChild] private unowned LibraryView library;
        [GtkChild] private unowned Gtk.Popover popover;
        [GtkChild] private unowned Gtk.Button edit_button;
        [GtkChild] private unowned Gtk.MenuButton main_button;


        construct {
            this.rebuild_ui();
            this.bind_property("media-only", this.library, "media-only", SYNC_CREATE);
            this.bind_property("hexpand", this.main_button, "hexpand", SYNC_CREATE);
        }


        private void rebuild_ui() {
            if (this.resource != null && this.resource.deleted) this.resource = null;

            if (this.resource != null) {
                Thumbnails.thumbnail_resource(this.thumbnail, this.resource, 0, 16);
                this.thumbnail.show();
                this.edit_button.show();
                this.name_label.label = this.resource.name;
                this.edit_button.tooltip_text = _("Edit %s").printf(this.resource.name);
            } else {
                this.thumbnail.hide();
                this.edit_button.hide();
                this.name_label.label = _("Select from library");
            }
        }


        [GtkCallback]
        private void on_resource_chosen(Cellar.Resource resource) {
            this.popover.popdown();
            this.resource = resource;
            this.resource_chosen(resource);
        }

        [GtkCallback]
        private void on_clear_clicked() {
            this.popover.popdown();
            this.resource = null;
            this.resource_chosen(null);
        }

        [GtkCallback]
        private void on_edit_clicked() {
            var info = new TabInfo.for_resource_editor(this.resource);
            PresenterConsole.get_instance().tabs.show_tab(info);
        }
    }
}
