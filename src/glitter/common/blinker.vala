namespace Glitter {
    public class Blinker : Gtk.DrawingArea {
        public const uint INTERVAL = 500;

        private bool _active = false;
        public bool active {
            get {
                return _active;
            }
            set construct {
                if (_active == value) return;

                _active = value;
                if (_active && this.source_id == 0) {
                    this.source_id = Timeout.add(INTERVAL, this.idle_func);
                }
            }
        }


        private bool on = false;

        private uint source_id = 0;


        construct {
            this.width_request = 12;
        }


        private bool idle_func() {
            if (this.active) {
                this.on = !this.on;
                this.queue_draw();
                return Source.CONTINUE;
            } else {
                this.on = false;
                this.queue_draw();
                this.source_id = 0;
                return Source.REMOVE;
            }
        }


        public override bool draw(Cairo.Context cr) {
            int width = this.get_allocated_width();
            int height = this.get_allocated_height();
            int size = width < height ? width : height;

            if (this.on) {
                cr.arc(width / 2.0, height / 2.0, size / 2.0, 0, Math.PI * 2);
                cr.set_source_rgb(1, 1, 1);
                cr.fill();
            }

            return true;
        }
    }
}
