using Gee;

namespace Cellar {
    public class Playlist : Object {
        public signal void segment_added(Segment segment, int index);

        public signal void segment_removed(int index);

        public signal void segment_moved(int from_index, int to_index);


        /**
         * The #PlaylistEntry for this #Playlist.
         */
        public PlaylistEntry entry { get; private set; }


        /**
         * The playlist's segments.
         */
        private Gee.List<Segment> segments;

        private Json.Object json;


        /**
         * Creates a #Playlist from the given file. If the file does not exist,
         * it will be created.
         */
        internal Playlist(PlaylistEntry entry) {
            this.entry = entry;

            this.segments = new Gee.ArrayList<Segment>();

            try {
                var parser = new Json.Parser();
                parser.load_from_file(this.entry.file.get_path());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                debug("Playlist %s does not exist, creating it now",
                      this.entry.uuid);
                this.json = new Json.Object();
                this.json.set_array_member("segments", new Json.Array());
            }

            this.json.get_array_member("segments").foreach_element(
                (array, index, val) => {
                    var segment = new Segment(val.get_object());
                    this.add_internal(segment, (int) index);
                }
            );
        }


        /**
         * Gets the list of segments in the playlist.
         */
        public Gee.List<Segment> get_segments() {
            return this.segments.read_only_view;
        }

        /**
         * Adds a segment to the playlist.
         * @index: index to insert the segment, or omit to insert at the end
         */
        public void insert_segment(Segment segment, int index=-1) {
            this.add_internal(segment, index);
            this.segment_added(segment, index);
            this.save();
        }

        /**
         * Convenience method for adding a header segment.
         *
         * Equivalent to:
         * ```vala
         * var segment = new Segment.header(name);
         * playlist.insert_segment(segment, index);
         * ```
         */
        public void insert_header(string name, int index=-1) {
            var segment = new Segment.header(name);
            this.insert_segment(segment, index);
        }

        /**
         * Repositions the segment at `from` to `to`. Both positions are
         * from before the change.
         */
        public void move_segment(int from, int to) {
            // both indexes are from before the change, so adjust in case
            // removing the segment changes the destination index
            int new_index = (from < to) ? to - 1 : to;

            Segment segment = this.segments[from];

            this.segments.remove(segment);
            this.segments.insert(new_index, segment);

            this.segment_moved(from, new_index);

            // save playlist
            this.save();
        }

        /**
         * Removes the segment at the given index.
         */
        public void remove_segment(int index) {
            this.remove_internal(index);
            this.segment_removed(index);
            this.save();
        }


        private void add_internal(Segment segment, int index) {
            segment.save_needed.connect(this.on_save_needed);
            if (index == -1) this.segments.add(segment);
            else this.segments.insert(index, segment);
        }

        private void remove_internal(int index) {
            Segment segment = this.segments[index];
            this.segments.remove_at(index);
            segment.save_needed.disconnect(this.on_save_needed);
        }

        /**
         * Signal handler for segments' save_needed signals
         */
        private void on_save_needed(Segment segment) {
            this.save();
        }

        /**
         * Saves the playlist to its file.
         */
        private void save() {
            // rebuild the segment list
            // this is more convenient than messing with Json.Array

            var array = new Json.Array();
            foreach (Segment segment in this.segments) {
                array.add_object_element(segment.json);
            }
            this.json.set_array_member("segments", array);

            Photon.save_json(this.entry.file, this.json);
        }
    }
}
