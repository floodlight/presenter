namespace Cellar {
    /**
     * Represents a group of slides.
     */
    public class Group : Object {
        public signal void slide_added(int index);

        public signal void slide_removed(int index);

        public signal void save_needed();


        public string uuid { get; private set; }

        /**
         * The #Slideshow this #Arrangement belongs to.
         */
        public weak Slideshow slideshow { get; private set; }

        public string name { get; set; }


        private Gee.List<Slide> slides;

        internal Json.Object json;


        /**
         * Creates a new #Group with the given UUID and JSON data.
         */
        internal Group(Slideshow slideshow, string uuid, Json.Object json) {
            this.slideshow = slideshow;
            this.uuid = uuid;
            this.json = json;

            this.slides = new Gee.ArrayList<Slide>();

            this.name = this.json.get_string_member("name");

            this.notify["name"].connect(() => {
                this.json.set_string_member("name", this.name);
                this.save_needed();
            });

            this.json.get_array_member("slides").foreach_element(
                (array, index, val) => {
                    var slide = new Slide(val.get_object());
                    this.add_slide_internal(slide);
                    this.slides.add(slide);
                }
            );
        }

        public Group.from_scratch(Slideshow slideshow) {
            var json = new Json.Object();
            json.set_string_member("name", _("New Group"));
            json.set_array_member("slides", new Json.Array());
            this(slideshow, Uuid.string_random(), json);

            this.insert_slide(new Cellar.Slide.blank(), 0);
        }


        /**
         * Gets a list of the slides in the group.
         */
        public Gee.List<Slide> get_slides() {
            return this.slides.read_only_view;
        }

        /**
         * Returns whether the given slide is the first slide in the group.
         */
        public bool is_first_slide(Slide slide) {
            if (this.slides.size == 0) return false;
            return this.slides[0] == slide;
        }

        /**
         * Inserts a slide into the group.
         *
         * If index is -1, the slide will be appended.
         */
        public void insert_slide(Slide slide, int index=-1) {
            if (index == -1) index = this.slides.size;

            this.slides.insert(index, slide);
            this.add_slide_internal(slide);
            this.slide_added(index);
            this.save();
        }

        /**
         * Removes the given slide from the group.
         */
        public void remove_slide(Slide slide) {
            int index = this.slides.index_of(slide);
            if (index == -1) return;

            this.slides.remove_at(index);
            this.remove_slide_internal(slide);
            this.slide_removed(index);

            if (this.slides.size == 0) {
                // Groups should never be empty, add a blank slide
                this.insert_slide(new Cellar.Slide.blank(), 0);
            }

            this.save();
        }

        /**
         * Returns the index of the slide in the group, or -1 if it is not in
         * the group.
         */
        public int index_of(Slide slide) {
            return this.slides.index_of(slide);
        }


        private void add_slide_internal(Slide slide) {
            slide.group = this;
            slide.save_needed.connect(this.on_slide_save_needed);
        }
        private void remove_slide_internal(Slide slide) {
            slide.group = null;
            slide.save_needed.disconnect(this.on_slide_save_needed);
        }
        private void on_slide_save_needed(Slide slide) {
            this.save();
        }

        private void save() {
            var slides = new Json.Array();
            foreach (Slide slide in this.slides) {
                slides.add_object_element(slide.json);
            }
            this.json.set_array_member("slides", slides);

            this.save_needed();
        }
    }
}
