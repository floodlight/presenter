namespace Cellar.ImportUtils {
    /**
     * Figures out what format a song file is in, then parses it using the
     * proper function.
     *
     * The resource is needed because some song formats contain metadata that
     * is included in the resource's fields.
     */
    public Slideshow parse_file(string file, Resource resource) {
        if (file.has_prefix("[File]")) return parse_ini_file(file, resource);
        else return parse_lyrics(file, resource);
    }

    /**
     * Parse a song in an unspecified format.
     *
     * We can assume a few things about the file, like that it will be split
     * into stanzas by double newlines and that there might be labels on the
     * stanzas. This function simply tries to accept whatever you throw at it
     * and come up with something reasonable.
     *
     * Postel's Law very much applies here.
     */
    public Slideshow parse_lyrics(string lyrics, Resource resource) {
        var slideshow = new Slideshow.from_scratch();

        // Start by splitting into stanzas
        string[] stanzas = stanzaify(lyrics);

        var groups = new Gee.HashMap<string, Cellar.Group>();

        int stanza_idx = -1;
        foreach (string stanza in stanzas) {
            stanza_idx ++;
            if (stanza_idx == 0 && stanza.index_of("\n") == -1) {
                // probably the song title
                resource.name = stanza;

                continue;
            }

            MatchInfo info;
            bool matches = /CCLI Song # (\d+)/.match(stanza, 0, out info);
            if (stanza_idx == stanzas.length - 1 && matches) {
                // CCLI information
                resource.set_field("meta:ccli", info.fetch(1));

                // next line seems to always be author(s)
                string[] lines = stanza.split("\n");
                if (lines.length >= 2) {
                    resource.set_field("meta:author", lines[1]);
                }

                continue;
            }

            // split the name from the stanza, if applicable
            string name = get_group_label(ref stanza);

            if (name != null && stanza == "") {
                // the stanza was just a group label. link that group here
                foreach (var group in groups.values) {
                    if (group.name == name) {
                        slideshow.arrangement.insert_group(group);
                        break;
                    }
                }
                continue;
            }

            if (groups.has_key(stanza)) {
                // the stanza is a duplicate. insert the same group again
                // instead of creating a new group
                var group = groups[stanza];
                slideshow.arrangement.insert_group(group);

                // name it "chorus <num>" if we haven't already
                if (group.name == null) {
                    group.name = find_unused_chorus_name(groups.values);
                }

                continue;
            }

            var group = slideshow.add_group();
            group.name = name;

            groups.set(stanza, group);

            string[] slides = slideify(stanza);
            foreach (string slide_text in slides) {
                Slide slide = new Slide.for_lyrics(slide_text);
                group.insert_slide(slide, -1);
            }

            // remove default slide from group
            group.remove_slide(group.get_slides()[0]);

            slideshow.arrangement.insert_group(group);
        }

        // Make sure all groups have names
        int group_num = 1;
        foreach (string stanza in stanzas) {
            var group = groups.get(stanza);
            if (group != null && group.name == null) {
                /* Translators: %d is a number */
                group.name = _("Group %d").printf(group_num++);
            }
        }

        return slideshow;
    }

    /**
     * Parse a file in an INI-like format used by some services.
     */
    public Slideshow parse_ini_file(string file, Resource resource) {
        string[] stanzas = {};
        string[] group_names = {};

        string[] lines = file.split("\n");
        foreach (string line in lines) {
            line = line.strip();
            if (line.contains("=")) {
                string[] keyval = line.split("=");
                string key = keyval[0].strip().casefold();
                string val = keyval[1].strip();

                switch (key) {
                    case "title":
                        resource.name = val;
                        break;
                    case "author":
                        resource.set_field("meta:author", val);
                        break;
                    case "copyright":
                        resource.set_field("meta:copyright", val);
                        break;
                    case "admin":
                        resource.set_field("meta:admin", val);
                        break;
                    case "keys":
                        resource.set_field("meta:keys", val);
                        break;
                    case "fields":
                        group_names = val.split("/t");
                        break;
                    case "words":
                        stanzas = val.split("/t");
                        break;
                }
            } else if (line.has_prefix("[S A")) {
                // CCLI number
                MatchInfo info;
                if (/\[S A(\d+)\]/.match(line, 0, out info)) {
                    resource.set_field("meta:ccli", info.fetch(1));
                }
            }
        }

        // do not create a slideshow from the resource, which is probably not
        // properly initialized yet
        var slideshow = new Slideshow.from_scratch();

        int i = 0;
        foreach (string stanza in stanzas) {
            if (stanza.strip() == "") continue;

            Group group = slideshow.add_group();
            if (group_names.length > i) group.name = group_names[i];
            else group.name = @"Group $(i)";

            string[] slides = slideify(stanza.replace("/n", "\n").strip());
            foreach (string slide_text in slides) {
                Slide slide = new Slide.for_lyrics(slide_text);
                group.insert_slide(slide, -1);
            }

            // remove default slide from group
            group.remove_slide(group.get_slides()[0]);

            slideshow.arrangement.insert_group(group);

            i ++;
        }

        return slideshow;
    }


    /**
     * Splits a stanza into one or more slides, depending on the
     * "import_lines_per_slide" config option.
     */
    private string[] slideify(string stanza) {
        string[] lines = stanza.split("\n");
        int max_lines = Photon.Config.get_instance().import_lines_per_slide;

        // This makes the slides a bit cleaner: if the lines will split evenly
        // between the number of slides needed, do so
        // for example, six lines with a max_lines of 4 will yield two 3-line
        // slides instead of a 4-line and a 2-line slide
        int slides_needed = (int) Math.ceil(lines.length / (double) max_lines);
        if (slides_needed > 0 && lines.length % slides_needed == 0) {
            max_lines = lines.length / slides_needed;
        }

        string[] slides = {};
        for (int i = 0; i < lines.length; i ++) {
            if (i % max_lines == 0) {
                // start new slide
                slides += "";
            }

            slides[slides.length - 1] = slides[slides.length - 1] + lines[i].strip() + "\n";
        }

        // strip trailing newlines
        for (int i = 0; i < slides.length; i ++) {
            slides[i] = slides[i].strip();
        }

        return slides;
    }

    /**
     * Splits a song into stanzas separated by double newlines.
     */
    private string[] stanzaify(string song) {
        string[] stanzas = /(\r?\n){2,}/.split(song);
        string[] result = {};

        // remove empty stanzas, strip extra whitespace from stanzas
        foreach (string stanza in stanzas) {
            if (stanza.strip() != "") result += stanza.strip();
        }

        return result;
    }

    /**
     * Detects whether the given string might be a group label. Returns the
     * label if it is, or null if it isn't.
     */
    private string? is_group_label(string line) {
        /* Translators: This is a list of group labels, separated by spaces. It
           is used for detecting labels in pasted lyrics. */
        string[] names = _("chorus verse pre-chorus prechorus bridge tag ending").split(" ");

        try {
            string name = /[\[\]:\d()]/.replace(line, line.length, 0, "").strip().casefold();
            if (name in names) {
                return /[\[\]:()]/.replace(line, line.length, 0, "").strip();
            }
        } catch (RegexError e) {
            // give up
        }

        return null;
    }

    /**
     * Looks for a group label on the first line of the stanza. If one is found,
     * it will be removed from the stanza.
     *
     * Returns null if no group label is found.
     */
    private string? get_group_label(ref string stanza) {
        string[] lines = stanza.split("\n");
        if (lines.length == 0) return null;

        string label = is_group_label(lines[0]);

        if (label != null) {
            stanza = string.joinv("\n", lines[1:lines.length]);
            return label;
        } else {
            return null;
        }
    }

    /**
     * Finds a chorus name that isn't used by one of the listed groups.
     */
    private string find_unused_chorus_name(Gee.Collection<Cellar.Group> groups) {
        /* Translators: %d is a number (so you have Chorus 1, Chorus 2, etc.) */
        string chorus = _("Chorus %d");

        for (int num = 1; ; num ++) {
            bool used = false;
            foreach (var group in groups) {
                if (group.name == chorus.printf(num)) {
                    used = true;
                    break;
                }
            }
            if (!used) return chorus.printf(num);
        }
    }
}
