namespace Cellar {
    /**
     * Represents a resource type, with its name, icon, etc.
     */
    public class ResourceType : Object {
        public static ResourceType VIDEO { get; private set; }
        public static ResourceType IMAGE { get; private set; }
        public static ResourceType SONG { get; private set; }
        public static ResourceType SLIDESHOW { get; private set; }


        public static ResourceType? for_id(string id) {
            switch (id) {
                case "video": return VIDEO;
                case "image": return IMAGE;
                case "song": return SONG;
                case "slideshow": return SLIDESHOW;
            }
            return null;
        }


        /**
         * Initializes the resource types.
         */
        internal static void init() {
            if (VIDEO != null) return;

            VIDEO = new ResourceType("video", "video-x-generic-symbolic");
            IMAGE = new ResourceType("image", "image-x-generic-symbolic");
            SONG = new ResourceType("song", "audio-x-generic-symbolic");
            SLIDESHOW = new ResourceType("slideshow", "x-office-presentation-symbolic");
        }


        /**
         * A string identifying the type
         */
        public string id { get; private set; }

        /**
         * The name of the symbolic icon that represents the type
         */
        public string icon { get; private set; }


        /**
         * Creates a new ResourceType
         */
        internal ResourceType(string id, string icon) {
            this.id = id;
            this.icon = icon;
        }


        /**
         * Whether this ResourceType is a slideshow (slideshow or song).
         */
        public bool is_slideshow() {
            return this == SLIDESHOW || this == SONG;
        }

        /**
         * Whether this ResourceType is a media file (video or image).
         */
        public bool is_media() {
            return this == VIDEO || this == IMAGE;
        }
    }
}
