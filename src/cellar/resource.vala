namespace Cellar {
    /**
     * Represents a resource in the library.
     */
    public class Resource : Object {
        internal signal void save_needed();


        /**
         * Emitted when the basic information about the resource (name,
         * subinfo, access time, etc.) changes, or when any of its fields
         * changes.
         */
        public signal void changed(Resource origin);

        /**
         * Emitted when the resource is removed from the library.
         */
        public signal void removed();

        /**
         * Emitted when a subresource is added
         */
        public signal void subresource_added(Resource subres);

        /**
         * Emitted when a subresource is removed
         */
        public signal void subresource_removed(Resource subres);


        /**
         * The unique identifier for this Resource. It is unique among all
         * shelves (and, in fact, all UUIDs ever created).
         */
        public string uuid { get; private set; }


        /**
         * The resource's name.
         */
        public string name { get; public set; }

        /**
         * A short string describing the resource in some way.
         *
         * The contents of this string depend on the type of resource. For
         * videos, this will be the length; for images, the dimensions.
         */
        public string? subinfo { get; public set; }

        /**
         * The type of resource that this is.
         */
        public ResourceType resource_type { get; private set; }

        /**
         * The last time the resource was accessed, in microseconds unix time.
         */
        public int64 last_access { get; private set; }

        /**
         * The file for this resource, if it has one.
         */
        public File file { get; private set; }

        /**
         * The file for this resource's thumbnail, if it has one.
         */
        public File thumbnail { get; private set; }

        /**
         * The parent resource, if this is a subresource.
         */
        public weak Resource? parent { get; private set; }

        /**
         * Whether the resource has been deleted.
         */
        public bool deleted { get; private set; default=false; }


        /**
         * The JSON object holding the information about this resource.
         */
        internal Json.Object json;

        private Slideshow? _slideshow;
        /**
         * The Slideshow for this Resource, if it is one of the slideshow types.
         */
        public Slideshow? slideshow {
            get {
                if (_slideshow == null) _slideshow = create_slideshow();
                return _slideshow;
            }
        }


        private Gee.HashMap<string, Resource> subresources;


        /**
         * Creates a Resource object for the given UUID and configuration.
         *
         * This is for instantiating stored resources, not creating new ones.
         * See Resource.from_scratch().
         *
         * All resources, no matter which shelf they are on, store their
         * configuration options in JSON.
         */
        internal Resource(string uuid, Json.Object json) {
            this.uuid = uuid;
            this.json = json;

            this.subresources = new Gee.HashMap<string, Resource>();

            this.name = json.get_string_member("name");
            this.subinfo = json.get_string_member("subinfo");
            this.resource_type = ResourceType.for_id(
                json.get_string_member("type")
            );
            this.last_access = json.get_int_member("last-access");

            if (json.has_member("subresources")) {
                json.get_object_member("subresources").foreach_member(
                    (obj, key, val) => {
                        var resource = new Resource(key, val.get_object());
                        this.add_subresource_internal(resource);
                    }
                );
            }

            this.notify["name"].connect((spec) => this.changed(this));
            this.notify["subinfo"].connect((spec) => this.changed(this));
            this.notify["last-access"].connect((spec) => this.changed(this));

            this.changed.connect(() => {
                this.json.set_string_member("name", this.name);
                this.json.set_string_member("subinfo", this.subinfo);
                this.json.set_int_member("last-access", this.last_access);

                this.save_needed();
            });


            this.file = get_resource_file(this.uuid);
            if (this.resource_type == ResourceType.IMAGE) {
                this.thumbnail = this.file;
            } else if (this.resource_type == ResourceType.VIDEO) {
                this.thumbnail = get_thumbnail_file(this.uuid);
            }
        }

        /**
         * Creates a new Resource
         */
        public Resource.from_scratch(ResourceType resource_type,
                                     string name,
                                     string? subinfo) {
            var json = new Json.Object();
            json.set_string_member("name", name);
            json.set_string_member("subinfo", subinfo);
            json.set_string_member("type", resource_type.id);
            json.set_int_member("last-access", get_real_time());

            this(Uuid.string_random(), json);
        }

        /**
         * THIS FUNCTION IS ONLY TO BE USED BY `Cellar.Slideshow` ITSELF.
         *
         * Creates a new Resource from a pre-existing Slideshow. The Slideshow
         * must not already belong to a resource.
         *
         * If @song is true, the resource will be a Song type rather than
         * Slideshow.
         */
        public Resource.from_slideshow(Slideshow slideshow, string name, bool song=false) {
            var type = song ? ResourceType.SONG : ResourceType.SLIDESHOW;
            this.from_scratch(type, name, null);

            if (slideshow.resource != null) {
                warning("This slideshow already belongs to a Resource!");
                return;
            }

            this._slideshow = slideshow;
        }

        /**
         * THIS FUNCTION IS ONLY TO BE USED BY `Cellar.Slideshow` ITSELF.
         *
         * Sets the resource's slideshow to the one given. This makes it
         * possible to link a resource with a slideshow after both have been
         * created.
         */
        public void connect_slideshow(Slideshow slideshow) {
            this._slideshow = slideshow;
        }


        /**
         * The full ID for the Resource. This is a path that includes all
         * parent resources, if this is a subresource.
         */
        public string get_full_id() {
            if (this.parent == null) return this.uuid;
            else return this.parent.get_full_id() + "/" + this.uuid;
        }

        /**
         * Gets a field from this Resource.
         *
         * Fields are a way of storing extra information about a resource,
         * beyond the basic metadata. Keys may be any string. Typical uses are
         * image orientation/rotation and video volume.
         */
        public string get_field(string key, string? default_val=null) {
            if (this.json.has_member("fields")) {
                Json.Object fields = this.json.get_object_member("fields");
                if (fields.has_member(key)) {
                    return fields.get_string_member(key);
                } else {
                    return default_val;
                }
            } else {
                return default_val;
            }
        }

        /**
         * Sets a field for this Resource.
         *
         * If new_val is null, the field will be unset.
         */
        public void set_field(string key, string? new_val) {
            Json.Object fields;
            if (this.json.has_member("fields")) {
                fields = this.json.get_object_member("fields");
            } else {
                fields = new Json.Object();
                this.json.set_object_member("fields", fields);
            }

            if (new_val == null) {
                fields.remove_member(key);
            } else {
                fields.set_string_member(key, new_val);
            }

            this.changed(this);
        }

        /**
         * Gets a subresource of this resource
         */
        public Resource? get_subresource(string uuid) {
            return this.subresources.get(uuid);
        }

        /**
         * Gets a list of all subresources
         */
        public Gee.Collection<Resource> get_subresources() {
            return this.subresources.values;
        }

        /**
         * Adds a resource as a subresource of this one
         */
        public void add_subresource(Resource res) {
            if (!this.json.has_member("subresources")) {
                this.json.set_object_member("subresources", new Json.Object());
            }

            this.json.get_object_member("subresources").set_object_member(
                res.uuid, res.json
            );

            this.add_subresource_internal(res);
            this.subresource_added(res);
            this.save_needed();
        }

        /**
         * Sets the last access time to the current time.
         */
        public void update_access_time() {
            this.last_access = get_real_time();
        }

        /**
         * Deletes all internal files associated with this resource, including
         * thumbnails and subresource files. Also emits the ::removed signal.
         */
        public void delete_files() {
            get_resource_file(this.uuid).delete_async.begin();
            get_thumbnail_file(this.uuid).delete_async.begin();

            foreach (var subres in this.subresources.values) {
                subres.delete_files();
            }

            this.deleted = true;
            this.removed();
        }


        private void add_subresource_internal(Resource subresource) {
            this.subresources[subresource.uuid] = subresource;
            subresource.parent = this;
            subresource.save_needed.connect(this.on_subresource_save_needed);
            subresource.changed.connect(this.on_subresource_changed);
        }

        private void remove_subresource_internal(string uuid) {
            Resource res;
            this.subresources.unset(uuid, out res);
            if (res != null) {
                res.parent = null;
                res.save_needed.disconnect(this.on_subresource_save_needed);
                res.changed.disconnect(this.on_subresource_changed);
            }
        }

        private void on_subresource_save_needed(Resource res) {
            this.save_needed();
        }
        private void on_subresource_changed(Resource res, Resource origin) {
            this.changed(origin);
        }

        /**
         * Gets the slideshow for this resource, if it is a %SLIDESHOW or
         * %SONG type.
         */
        private Slideshow? create_slideshow() {
            // Normally, you would want to use a WeakRef or similar structure
            // to ensure that only one instance of the slideshow is active at
            // once, but it also gets destroyed when no longer needed.
            // But we're not going to bother with that. There's not that many
            // slideshows in the average library so we'll keep them around
            // forever to cut down a bit on complexity.
            // related and funny: https://groups.google.com/forum/message/raw?msg=comp.lang.ada/E9bNCvDQ12k/1tezW24ZxdAJ

            if (this.resource_type != ResourceType.SONG &&
                this.resource_type != ResourceType.SLIDESHOW) {
                warning("Tried to get the slideshow for a %s resource!",
                        this.resource_type.id);
                return null;
            }

            if (this.file == null) {
                warning("Cannot load slideshow because resource has no URI! " +
                    "You probably need to add it to a PlaylistIndex first.");
                return null;
            }

            return new Slideshow.from_resource(this);
        }
    }
}
