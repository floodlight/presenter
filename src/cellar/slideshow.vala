using Gee;

namespace Cellar {
    public class Slideshow : Object {
        /**
         * Emitted when a group is added or removed.
         */
        public signal void groups_changed();

        /**
         * Emitted when a slide is added to a group.
         */
        public signal void slide_added(Group group, int index);

        /**
         * Emitted when a slide is removed from a group.
         */
        public signal void slide_removed(Group group, int index);


        /**
         * Emitted when the slideshow's JSON data needs to be saved.
         *
         * Will not be emitted if the slideshow was loaded with
         * Slideshow.from_resource(). In that case, the slideshow will handle
         * saving itself.
         */
        internal signal void save_needed();

        /**
         * The arrangement of the slides in the slideshow.
         */
        public Arrangement arrangement { get; private set; }

        /**
         * The resource this slideshow is loaded from, if it is loaded from a
         * resource. If it is null, it means the slideshow is internal to the
         * playlist.
         */
        public Resource? resource { get; private set; }

        /**
         * Whether the slideshow is internal. Shortcut for `resource == null`.
         */
        public bool is_internal {
            get {
                return this.resource == null;
            }
        }


        /**
         * Map of groups by their UUIDs
         */
        private HashMap<string, Group> groups;

        internal Json.Object json;


        /**
         * Creates a new Slideshow.
         */
        internal Slideshow(Json.Object json) {
            this.json = json;

            this.groups = new HashMap<string, Group>();

            this.json.get_object_member("groups").foreach_member(
                (obj, key, val) => {
                    var group = new Group(this, key, val.get_object());
                    this.add_group_internal(group);
                    this.groups[key] = group;
                }
            );

            var arrangement = this.json.get_object_member("arrangement");
            this.arrangement = new Arrangement(this, arrangement);
            this.arrangement.save_needed.connect(() => this.save());
        }

        /**
         * Loads a Slideshow from a resource. The slideshow will be saved
         * directly to the resource file when it changes, rather than needing
         * to listen to the ::save_needed signal.
         *
         * If the resource's file does not exist, it will be created.
         */
        public Slideshow.from_resource(Resource resource) {
            File file = resource.file;
            Json.Object json;
            if (file.query_exists()) {
                try {
                    var parser = new Json.Parser();
                    parser.load_from_file(file.get_path());
                    json = parser.get_root().get_object();
                } catch (Error e) {
                    warning("Could not load slideshow from %s!",
                            file.get_path());
                    return;
                }
            } else {
                // Slideshow does not exist yet, create it
                debug("Slideshow file %s does not exist, creating it now",
                        file.get_path());
                json = load_default_json();
            }

            this(json);
            this.resource = resource;
            this.save();
        }

        /**
         * Creates a Slideshow with a single slide displaying the given media
         * resource.
         */
        public Slideshow.from_media(Resource resource) {
            Json.Object json = load_default_json();
            this(json);

            var slide = new Slide.for_media(resource);
            this.arrangement.insert_slide(slide);
        }

        /**
         * Loads the default JSON for a slideshow.
         */
        private static Json.Object load_default_json() {
            Json.Object json = null;

            try {
                File defaults = File.new_for_uri("resource:///io/gitlab/floodlight/Presenter/defaults/slideshow.json");
                var parser = new Json.Parser();
                parser.load_from_stream(defaults.read());
                json = parser.get_root().get_object();
            } catch (Error e) {
                critical("ERROR LOADING NEW SLIDESHOW: %s", e.message);
            }

            return json;
        }

        /**
         * Creates a new Slideshow.
         */
        public Slideshow.from_scratch() {
            this(load_default_json());
        }


        /**
         * Gets a group by UUID.
         */
        public Group get_group(string uuid) {
            return this.groups.get(uuid);
        }

        /**
         * Gets a read-only list of all groups.
         */
        public Gee.Collection<Group> get_groups() {
            return this.groups.values;
        }

        /**
         * Creates a new, empty Group and adds it to the slideshow.
         */
        public Group add_group() {
            var group = new Group.from_scratch(this);

            this.groups[group.uuid] = group;
            this.json.get_object_member("groups").set_object_member(
                group.uuid, group.json
            );
            this.add_group_internal(group);

            this.groups_changed();
            this.save();

            return group;
        }

        /**
         * Adds the slideshow to the Library as a resource. Returns the newly
         * created Resource.
         */
        public Cellar.Resource add_to_library(string name, bool song=false) {
            var res = new Cellar.Resource.from_slideshow(this, name, song);
            Cellar.Library.get_instance().user_shelf.add_resource(res);

            this.resource = res;
            this.save();

            return res;
        }

        /**
         * Adds the slideshow to the Library as a resource. The resource must
         * be provided.
         */
        public void add_to_library_with_resource(Cellar.Resource res) {
            res.connect_slideshow(this);
            Cellar.Library.get_instance().user_shelf.add_resource(res);
            this.resource = res;
            this.save();
        }


        private void on_slide_added_to_group(Group group, int index) {
            this.slide_added(group, index);
        }
        private void on_slide_removed_from_group(Group group, int index) {
            this.slide_removed(group, index);
        }

        private void add_group_internal(Group group) {
            group.save_needed.connect(this.on_group_save_needed);
            group.slide_added.connect(this.on_slide_added_to_group);
            group.slide_removed.connect(this.on_slide_removed_from_group);
        }
        private void remove_group_internal(Group group) {
            group.save_needed.disconnect(this.on_group_save_needed);
            group.slide_added.disconnect(this.on_slide_added_to_group);
            group.slide_removed.disconnect(this.on_slide_removed_from_group);
        }
        private void on_group_save_needed(Group group) {
            this.save();
        }

        /**
         * If the slideshow was loaded from a resource file, saves it.
         * Otherwise, emits the ::save_needed signal.
         */
        private void save() {
            if (this.resource != null) Photon.save_json(this.resource.file, this.json);
            else this.save_needed();
        }
    }
}
