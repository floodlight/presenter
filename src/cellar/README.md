# Cellar: Floodlight Data Storage

> No one, when he has lit a lamp, puts it in a cellar or under a basket, but
on a stand, that those who come in may see the light.  
Luke 11:33

Cellar is where Floodlight stores its files. This module handles the library,
thumbnails, and the playlist index.
