namespace Halogen {
    /**
     * Manages the state of the Floodlight instance: what playlist is open,
     * what segment/slide is on the screen, stage messages, etc.
     */
    public class State : Object {
        private static State instance;
        public static State get_instance() {
            if (instance == null) instance = new State();
            return instance;
        }


        /**
         * Emitted when the current slide changes. This includes when the
         * current slide is restarted.
         */
        public signal void slide_changed();

        /**
         * Emitted when the user changes the slide (by clicking its icon,
         * arrow keys, remote control, etc.) but not when the slide advances
         * by itself.
         *
         * Will be emitted after slide_changed().
         */
        public signal void user_changed_slide();


        private Cellar.Playlist _playlist;
        /**
         * The currently open playlist.
         *
         * Setting this will also set the open_playlist property of the
         * PlaylistIndex.
         */
        public Cellar.Playlist playlist {
            get {
                return _playlist;
            }
            set {
                if (this._playlist != null) {
                    this._playlist.segment_added.disconnect(this.on_segment_added);
                    this._playlist.segment_removed.disconnect(this.on_segment_removed);
                    this._playlist.segment_moved.disconnect(this.on_segment_moved);
                }

                this._playlist = value;
                Cellar.PlaylistIndex.get_instance().open_playlist = this.playlist.entry;

                this._playlist.segment_added.connect(this.on_segment_added);
                this._playlist.segment_removed.connect(this.on_segment_removed);
                this._playlist.segment_moved.connect(this.on_segment_moved);

                this.segment_index = 0;
                this.slide_index = 0;

                this.floating = true;
                int segment_index, slide_index;
                this.get_next(out segment_index, out slide_index);

                // if the playlist is empty, leave segment,slide at 0,0
                if (slide_index != -1) {
                    this.segment_index = segment_index;
                    this.slide_index = slide_index;
                }

                this.update_internal();
                this.slide_changed();
            }
        }

        /**
         * The index, within the current playlist, of the current segment.
         */
        public int segment_index { get; private set; }
        /**
         * The current segment.
         */
        public Cellar.Segment segment { get; private set; }

        /**
         * The index, within the current segment, of the current slide.
         */
        public int slide_index { get; private set; }
        /**
         * The current slide.
         */
        public Cellar.Slide slide { get; private set; }

        /**
         * The current ResourcePlayerPool to use for foregrounds.
         */
        public ResourcePlayerPool pool;

        /**
         * If Floodlight is in a "floating" state, then no slide is being
         * displayed. This can happen if the current slide or segment is
         * removed. In this state, the "current" slide is actually the next
         * slide (the one that will be on screen when next() is called).
         */
        public bool floating { get; set; default=false; }


        private State() {
            this.playlist = Cellar.PlaylistIndex.get_instance().open_playlist.get_playlist();
        }


        /**
         * Advances to the next slide. Does nothing if there is no next slide.
         */
        public void next() {
            int segment;
            int slide;
            this.get_next(out segment, out slide);

            if (slide != -1) this.set_position(segment, slide);
        }

        /**
         * Goes back to the previous slide. Does nothing if there is no previous
         * slide.
         */
        public void prev() {
            int segment;
            int slide;
            this.get_prev(out segment, out slide);

            if (slide != -1) this.set_position(segment, slide);
        }

        private Gee.List<Cellar.Slide> slides_in_segment(Cellar.Segment segment) {
            if (segment.segment_type == SLIDESHOW) {
                return segment.slideshow.arrangement.get_slides();
            } else {
                return new Gee.ArrayList<Cellar.Slide>();
            }
        }

        /**
         * Gets the next slide and its segment. Both arguments will be set to
         * -1 if there is no next slide. If the state is floating, returns the
         * current slide (which is not being displayed).
         */
        public void get_next(out int segment, out int slide) {
            Gee.List<Cellar.Segment> segments = this.playlist.get_segments();

            segment = this.segment_index;
            slide = this.slide_index;

            // make sure current index is valid, otherwise this won't work
            if (segment < 0 || segments.size <= segment) {
                segment = -1;
                slide = -1;
                return;
            }

            Gee.List<Cellar.Slide> slides = this.slides_in_segment(segments[segment]);

            if (this.floating) {
                if (slides.size > slide && slides[slide].enabled) {
                    // we're already floating on a valid slide
                    return;
                }
            }

            while (true) {
                slide ++;
                if (slide >= slides.size) {
                    slide = 0;

                    // if the current segment doesn't loop, go to the next one
                    if (!segments[segment].looping || segments[segment].is_empty()) {
                        segment ++;
                        if (segment >= segments.size) {
                            // we're at the last segment and no more slides
                            segment = -1;
                            slide = -1;
                            return;
                        }

                        slides = this.slides_in_segment(segments[segment]);
                    }
                }

                if (slides.size != 0 && slides[slide].enabled) {
                    // we've found a valid next slide
                    return;
                }
            }
        }

        /**
         * Gets the previous slide and its segment. Both arguments will be set
         * to -1 if there is no previous slide.
         */
        public void get_prev(out int segment, out int slide) {
            Gee.List<Cellar.Segment> segments = this.playlist.get_segments();

            segment = this.segment_index;
            slide = this.slide_index;

            // make sure current index is valid, otherwise this won't work
            if (segment < 0 || segments.size <= segment) {
                segment = -1;
                slide = -1;
                return;
            }

            Gee.List<Cellar.Slide> slides = this.slides_in_segment(
                this.playlist.get_segments()[segment]
            );

            while (true) {
                slide --;

                if (slide < 0) {
                    if (!segments[segment].looping || segments[segment].is_empty()) {
                        segment --;
                        if (segment < 0) {
                            // no previous slide
                            segment = -1;
                            slide = -1;
                            return;
                        }

                        slides = this.slides_in_segment(segments[segment]);
                    }

                    slide = slides.size - 1;
                }

                if (slides.size != 0 && slides[slide].enabled) {
                    // we've found a valid next slide
                    return;
                }
            }
        }

        /**
         * Sets the current segment and slide index. This will emit
         * user_changed_slide().
         */
        public void set_position(int segment_index, int slide_index=-1) {
            int segment_count = this.playlist.get_segments().size;
            if (segment_index >= segment_count) {
                warning("Segment index %d out of range %d!", segment_index, segment_count);
                return;
            }

            Cellar.Segment segment = this.playlist.get_segments()[segment_index];
            if (slide_index == -1) {
                bool slide_found = false;
                foreach (Cellar.Slide slide in segment.slideshow.arrangement.get_slides()) {
                    slide_index ++;
                    if (slide.enabled) {
                        slide_found = true;
                        break;
                    }
                }
                if (!slide_found) return;
            }

            int slide_count = segment.slideshow.arrangement.get_slides().size;
            if (slide_index >= slide_count) {
                warning("Slide index %d.%d out of range %d!", segment_index, slide_index, slide_count);
                return;
            }

            this.segment_index = segment_index;
            this.slide_index = slide_index;

            if (this.pool != null) {
                this.pool.pause(true);
            }
            // Create a new ResourcePlayerPool
            this.pool = new ResourcePlayerPool();

            this.floating = false;
            this.update_internal();
            if (this.slide.continue_mode == WHEN_FINISHED) {
                this.pool.finished.connect(() => this.next());
            } else if (this.slide.continue_mode == TIMER) {
                this.pool.fixed_duration = this.slide.continue_delay;
                this.pool.finished.connect(() => this.next());
            }
            this.slide_changed();
            this.user_changed_slide();
        }


        /**
         * Makes sure the internal state is consistent (sets `segment` and
         * `slide`, hooks up signal handlers, etc.)
         */
        private void update_internal() {
            debug("Current state: %d/%d %s", this.segment_index, this.slide_index, this.floating ? "floating" : "");

            if (this.segment != null) {
                this.segment.slideshow.arrangement.slides_spliced.disconnect(this.on_slides_spliced);
                this.segment.notify["arrangement"].disconnect(this.on_arrangement_changed);
            }

            if (this.floating) {
                this.segment = null;
                this.slide = null;
            } else {
                this.segment = this.playlist.get_segments()[this.segment_index];
                this.segment.slideshow.arrangement.slides_spliced.connect(this.on_slides_spliced);
                this.segment.notify["arrangement"].connect(this.on_arrangement_changed);
                this.slide = this.segment.slideshow.arrangement.get_slides()[this.slide_index];
            }
        }

        private void on_segment_added(Cellar.Segment segment, int index) {
            // edge case: if the state is 0/0 floating, stay there
            if (this.segment_index == 0 && this.slide_index == 0 && this.floating) return;

            if (this.segment_index >= index) this.segment_index ++;
        }

        private void on_segment_removed(int index) {
            if (this.segment_index > index) {
                this.segment_index --;
            } else if (this.segment_index == index) {
                this.floating = true;
                this.update_internal();
                this.slide_changed();
            }
        }

        private void on_segment_moved(int from, int to) {
            if (this.segment_index == from) {
                this.segment_index = to;
            } else if (this.segment_index < from) {
                if (this.segment_index >= to) {
                    this.segment_index ++;
                }
            } else {
                if (this.segment_index <= to) {
                    this.segment_index --;
                }
            }

            this.update_internal();
        }

        /**
         * Signal handler for when the current arrangement's slides are changed.
         */
        private void on_slides_spliced(int index, int added, int removed) {
            if (this.slide_index >= index && this.slide_index < index + removed) {
                this.slide_index = index;
                this.floating = true;

                this.update_internal();
                this.slide_changed();
            } else if (this.slide_index >= index) {
                this.slide_index += (added - removed);
                this.update_internal();
            }
        }

        private void on_arrangement_changed() {
            int i = 0;
            bool slide_matched = false;
            foreach (var slide in this.segment.slideshow.arrangement.get_slides()) {
                if (slide == this.slide) {
                    slide_matched = true;
                    break;
                }

                i ++;
            }

            if (slide_matched) {
                this.slide_index = i;
            } else {
                this.slide_index = 0;
                this.floating = true;
            }

            this.update_internal();
            this.slide_changed();
        }
    }
}
