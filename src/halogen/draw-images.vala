/**
 * Drawing utils for Halogen.
 */
namespace Halogen {
    /**
     * Specifies how an image should be scaled to fit a differently sized area.
     */
    public enum ScaleOpts {
        /**
         * Scale the image so that it fits entirely and perfectly in the target
         * area.
         */
        FIT,

        /**
         * Scale the image so that it entirely fills the target area.
         */
        FILL,

        /**
         * Stretches the image to the target area, disregarding aspect ratio.
         */
        STRETCH,

        /**
         * If the image is smaller than the target, center it within the target.
         * If it is larger, apply SCALE_TO_FILL.
         */
        SHRINK;

        public void apply(ref double w, ref double h,
                          double tw, double th) {
            switch (this) {
                case FIT:
                    scale_to_fit(ref w, ref h, tw, th);
                    break;
                case FILL:
                    scale_to_fill(ref w, ref h, tw, th);
                    break;
                case STRETCH:
                    w = tw;
                    h = th;
                    break;
                case SHRINK:
                    shrink_to_fit(ref w, ref h, tw, th);
                    break;
            }
        }
    }

    /**
     * Scales the rectangle (w, h) up or down to fit perfectly in (tw, th).
     */
    public void scale_to_fit(ref double w, ref double h,
                             double tw, double th) {
        double ratio = w / h;
        double t_ratio = tw / th;
        if (t_ratio < ratio) {
            w = tw;
            h = tw * (1 / ratio);
        } else {
            w = th * ratio;
            h = th;
        }
    }

    /**
     * Scales the rectangle (w, h) to completely fill (tw, th).
     */
    public void scale_to_fill(ref double w, ref double h,
                              double tw, double th) {
        double ratio = w / h;
        double t_ratio = tw / th;
        if (t_ratio > ratio) {
            w = tw;
            h = tw * (1 / ratio);
        } else {
            w = th * ratio;
            h = th;
        }
    }

    /**
     * Same as scale_to_fit(), but does not make the rectangle bigger; if the
     * first rectangle is smaller than the second, no change will be made.
     */
    public void shrink_to_fit(ref double w, ref double h,
                              double tw, double th) {
        double nw = w, nh = h;
        scale_to_fit(ref nw, ref nh, tw, th);
        if (nw < w) {
            w = nw;
            h = nh;
        }
    }

    /**
     * Finds the amount to translate the first rectangle so that it is centered
     * on the second.
     */
    public void center(double w, double h,
                       double tw, double th,
                       out double out_x, out double out_y) {
        out_x = tw / 2 - w / 2;
        out_y = th / 2 - h / 2;
    }


    /**
     * Specifies the transforms that should be applied when drawing a surface
     * (rotation, flipping, scaling option, etc.)
     */
    public class DrawOpts {
        /**
         * How the image should be scaled.
         */
        public ScaleOpts scale;

        /**
         * How much to rotate the image, in degrees. Must be 0 or a positive
         * multiple of 90.
         */
        public int rotation;

        /**
         * Whether to flip the image horizontally
         */
        public bool hflip;

        /**
         * Whether to flip the image vertically
         */
        public bool vflip;


        public DrawOpts(ScaleOpts scale = FIT,
                        int rotation = 0,
                        bool hflip = false,
                        bool vflip = false) {
            this.scale = scale;
            this.rotation = rotation;
            this.hflip = hflip;
            this.vflip = vflip;
        }

        /**
         * Creates a new DrawOpts using the fields in the given resource.
         */
        public DrawOpts.from_resource(Cellar.Resource resource) {
            this.scale = (ScaleOpts) int.parse(resource.get_field("scale", "0"));
            this.rotation = int.parse(resource.get_field("rotate", "0"));
            this.hflip = resource.get_field("hflip", "false") == "true";
            this.vflip = resource.get_field("vflip", "false") == "true";
        }
    }


    /**
     * Draws the given surface in the given rectangle using the given drawing
     * options.
     *
     * Enabling `smooth` is a bad idea because it has a severe impact on
     * performance. It should only be enabled on stuff like thumbnails.
     */
    public void draw_to_context(
        Cairo.Context cr, Cairo.ImageSurface surf,
        double x, double y, double width, double height,
        DrawOpts? draw_opts = null, double alpha = 1, bool smooth = false
    ) {
        int img_w = surf.get_width();
        int img_h = surf.get_height();
        cr.set_source_surface(surf, 0, 0);
        draw_to_context_internal(cr, img_w, img_h, x, y, width, height, draw_opts, alpha, smooth);
    }

    /**
     * Like draw_to_context(), but for pixbufs instead of image surfaces.
     */
    public void draw_pixbuf_to_context(
        Cairo.Context cr, Gdk.Pixbuf pixbuf,
        double x, double y, double width, double height,
        DrawOpts? draw_opts = null, double alpha = 1, bool smooth = false
    ) {
        int surf_w = pixbuf.get_width();
        int surf_h = pixbuf.get_height();
        Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
        draw_to_context_internal(cr, surf_w, surf_h, x, y, width, height, draw_opts, alpha, smooth);
    }

    /**
     * Draws to the Cairo context. This is used by draw_to_context() and
     * draw_pixbuf_to_context().
     */
    private void draw_to_context_internal(
        Cairo.Context cr,
        int img_w, int img_h,
        double x, double y, double width, double height,
        DrawOpts? draw_opts = null, double alpha = 1, bool smooth = false
    ) {
        cr.save();

        DrawOpts opts = (draw_opts == null) ? new DrawOpts() : draw_opts;

        int img_w_old = img_w;
        int img_h_old = img_h;
        if (opts.rotation == 90 || opts.rotation == 270) {
            int tmp = img_w;
            img_w = img_h;
            img_h = tmp;
        }

        double tw = img_w;
        double th = img_h;
        opts.scale.apply(ref tw, ref th, width, height);

        if (!smooth) {
            cr.get_source().set_filter(Cairo.Filter.FAST);
        }

        Cairo.Matrix matrix;
        cr.get_source().get_matrix(out matrix);

        // Flips and rotations must be around the center, hence the translate
        // lines. The first translation must use the original width and height,
        // not rotated width and height.
        matrix.translate(img_w_old / 2, img_h_old / 2);
        if (opts.hflip) matrix.scale(-1, 1);
        if (opts.vflip) matrix.scale(1, -1);
        matrix.rotate(-opts.rotation * Math.PI / 180);
        matrix.translate(-img_w / 2, -img_h / 2);

        matrix.scale(img_w / tw, img_h / th);
        matrix.translate(-x, -y);

        double tx, ty;
        center(tw, th, width, height, out tx, out ty);
        matrix.translate(-tx, -ty);

        cr.get_source().set_matrix(matrix);

        cr.translate(x, y);

        if (alpha != 1) {
            cr.paint_with_alpha(alpha);
        } else {
            cr.paint();
        }
        cr.restore();
    }
}
