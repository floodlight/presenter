namespace Halogen {
    /**
     * Stage view feeds are much more complex than normal feeds, so they get
     * their own class.
     */
    public class StageViewFeed : Feed {
        private const double SIDEBAR_SPLIT = 1.0 / 3;
        private const int SIDEBAR_MARGIN = 50;

        private TextOptions clock_font;
        private TextOptions lyrics_font;
        private TextOptions lyrics_next_font;
        private TextOptions comments_font;

        /**
         * The current LayoutRenderer.
         */
        private LayoutRenderer? renderer;

        /**
         * The thumbnail LayoutRenderer for the next slide.
         */
        private LayoutRenderer? next_renderer;

        public StageViewFeed(string id, Json.Object json) {
            base(id, json);

            this.clock_font = new TextOptions.defaults_clock();
            this.lyrics_font = new TextOptions.defaults_stageview();
            this.lyrics_next_font = new TextOptions.defaults_stageview_next();
            this.comments_font = new TextOptions.defaults_comments();

            State.get_instance().slide_changed.connect(this.update_slide);
            this.update_slide();
        }


        public override void render(Cairo.Context cr, int x, int y) {
        lock (this.renderer) {
            if (this.width == 0 || this.height == 0) return;

            if (this.renderer == null) {
                cr.set_source_rgb(0, 0, 0);
                cr.rectangle(x, y, this.width, this.height);
                cr.fill();

                this.draw_sidebar(cr, x, y, this.width, this.height);
                return;
            }

            switch (this.renderer.page.stage_view_mode) {
                case NONE:
                    cr.set_source_rgb(0, 0, 0);
                    cr.rectangle(x, y, this.width, this.height);
                    cr.fill();

                    this.draw_sidebar(cr, x, y, this.width, this.height);

                    Cellar.Slide slide = State.get_instance().slide;
                    Cellar.Segment segment = State.get_instance().segment;
                    y += this.draw_slide_preview(cr, segment, slide, x, y, this.width, this.height, true);

                    int next_segment, next_slide;
                    State.get_instance().get_next(out next_segment, out next_slide);

                    if (next_slide != -1) {
                        segment = State.get_instance().playlist.get_segments()[next_segment];
                        slide = segment.slideshow.arrangement.get_slides()[next_slide];
                        this.draw_slide_preview(cr, segment, slide, x, y, this.width, this.height, false);
                    }

                    break;
                case PARTIAL:
                    cr.set_source_rgb(0, 0, 0);
                    cr.rectangle(x, y, this.width, this.height);
                    cr.fill();

                    this.renderer.render(cr, x, y, (int) (this.width * (1 - SIDEBAR_SPLIT)), this.height);
                    this.draw_sidebar(cr, x, y, this.width, this.height);
                    break;
                case FULL:
                    this.renderer.render(cr, x, y, this.width, this.height);
                    break;
            }
        }
        }

        public override void resize(int new_width, int new_height) {
            base.resize(new_width, new_height);
        }


        /**
         * Draws the sidebar on the context.
         *
         * Given dimensions should be those of the entire feed, not just the
         * sidebar.
         */
        private void draw_sidebar(Cairo.Context cr, int x, int y, int w, int h) {
            cr.save();

            x += (int) (w * (1 - SIDEBAR_SPLIT)) + SIDEBAR_MARGIN;
            w = (int) (w * SIDEBAR_SPLIT) - SIDEBAR_MARGIN;

            // Set clip
            cr.rectangle(x, y, w, h);
            cr.clip();

            // Clock
            string clock = new DateTime.now().format("%T");
            y += (int) render_text(clock, cr, this.clock_font, x, y, w, h * 0.1, h);

            // Segment comments
            Cellar.Segment segment = State.get_instance().segment;
            if (segment != null && segment.comments != null) {
                y += (int) render_text(
                    segment.comments, cr, this.comments_font,
                    x, y, w, h * 0.1, h
                );
            }

            cr.restore();
        }

        /**
         * Draws a slide preview for the normal stage view.
         */
        private int draw_slide_preview(Cairo.Context cr,
                                       Cellar.Segment segment,
                                       Cellar.Slide slide,
                                       int x, int y, int w, int h,
                                       bool top) {

            int width = (int) (this.width * (1 - SIDEBAR_SPLIT));
            int height;

            string content = slide.get_field("content");
            Layout layout = LayoutManager.get_instance().get_layout(slide.layout_id);
            if (content != null && layout.has_field("content")) {
                TextOptions font = top ? this.lyrics_font : this.lyrics_next_font;

                height = (int) render_text(
                    content, cr, font,
                    x, y, width, this.height,
                    this.height
                );
            } else if (top) {
                height = (int) (this.height * (1 - SIDEBAR_SPLIT));
                this.renderer.render(cr, x, y, width, height);
            } else {
                // thumbnail
                if (this.next_renderer == null) {
                    this.next_renderer = layout.page_for_feed(this.id).get_thumbnail_renderer(segment, slide);
                }

                width = (int) (this.width * SIDEBAR_SPLIT);
                height = (int) (this.height * SIDEBAR_SPLIT);
                this.next_renderer.render(cr, x, y, width, height);
            }

            return height;
        }


        private void update_slide() {
            Cellar.Segment segment = State.get_instance().segment;
            Cellar.Slide slide = State.get_instance().slide;

            lock (this.renderer) {
                if (slide == null) {
                    this.renderer = null;
                } else {
                    Layout layout = LayoutManager.get_instance().get_layout(slide.layout_id);
                    this.renderer = layout.page_for_feed(this.id).get_renderer(segment, slide);
                    // will be created if needed
                    this.next_renderer = null;
                }
            }
        }
    }
}
