namespace Halogen {
    /**
     * Base class for resource players. Subclassed by ImageResourcePlayer and
     * VideoResourcePlayer.
     */
    public abstract class ResourcePlayer : Object {
        /**
         * The resource that the ResourcePlayer is playing
         */
        public Cellar.Resource resource { get; private set; }

        /**
         * The output buffer.
         */
        public DoubleBuffer buffer { get; protected set; }

        /**
         * Whether the resource player should be muted.
         */
        public virtual bool mute { get; set; default=true; }


        /**
         * The pool that created the ResourcePlayer.
         */
        protected ResourcePlayerPool pool;


        public static ResourcePlayer? get_player(ResourcePlayerPool pool,
                                                 Cellar.Resource resource,
                                                 bool thumbnail) {
            if (thumbnail) {
                return new ImagePlayer(pool, resource, true);
            } else if (resource.resource_type == Cellar.ResourceType.IMAGE) {
                return new ImagePlayer(pool, resource);
            } else if (resource.resource_type == Cellar.ResourceType.VIDEO) {
                return new VideoPlayer(pool, resource);
            } else {
                return null;
            }
        }


        protected ResourcePlayer(ResourcePlayerPool pool, Cellar.Resource resource) {
            this.pool = pool;
            this.resource = resource;
            this.buffer = new DoubleBuffer(1, 1);
        }


        /**
         * Returns the ResourcePlayer to the pool it was created by. May also
         * return resources used by the player.
         */
        public void return_to_pool() {
            this.pool.return_player(this.resource);
        }

        /**
         * Tells the ResourcePlayer that it is no longer needed and should
         * destroy any resources it was using.
         */
        public virtual void destroy() {}

        /**
         * Sets whether the player is paused, if applicable.
         */
        public virtual void set_paused(bool paused) {}

        /**
         * Gets the time position of the player, or -1 if not applicable.
         */
        public virtual double get_position() { return -1; }

        /**
         * Seeks to the given time position in the player, if applicable.
         */
        public virtual void seek(double position) {}
    }


    public class ImagePlayer : ResourcePlayer {
        public ImagePlayer(ResourcePlayerPool pool,
                           Cellar.Resource resource,
                           bool thumbnail = false) {
            base(pool, resource);

            try {
                File file = thumbnail ? resource.thumbnail : resource.file;
                var pixbuf = new Gdk.Pixbuf.from_file(file.get_path());

                this.buffer = new DoubleBuffer(pixbuf.width, pixbuf.height);
                this.buffer.with_back((back) => {
                    var cr = new Cairo.Context(back);
                    Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0);
                    cr.paint();
                });
            } catch (Error err) {
                // image failed to load
                // not much we can do about it
                warning("Error loading image! %s", err.message);

                this.buffer.with_back((back) => {
                    var cr = new Cairo.Context(back);
                    cr.set_source_rgba(0, 0, 0, 0);
                    cr.paint();
                });
            }

            this.buffer.swap();
        }
    }


    /**
     * Plays a video resource using GStreamer.
     */
    public class VideoPlayer : ResourcePlayer {
        private bool _mute = true;
        public override bool mute {
            get {
                return _mute;
            }
            set {
                _mute = value;
                this.update_volume();
            }
        }


        /**
         * The duration of the resource.
         */
        public double duration { get; private set; }


        /**
         * The pipeline containing all elements needed for the player to work.
         */
        private Gst.Pipeline pipeline;

        /*
         * Decodes the video file.
         */
        private dynamic Gst.Element uridecodebin;

        /**
         * Makes sure the video is compatible with the format we need (ARGB32)
         */
        private Gst.Element videoconvert;
        private Gst.Element audioconvert;

        private dynamic Gst.Element volume;
        private dynamic Gst.Element audiosink;

        private Gst.Bus bus;

        /**
         * The appsink that provides the video data to the VideoPlayer
         */
        private Gst.App.Sink appsink;

        private bool buffer_initialized = false;

        private string end_mode;


        internal VideoPlayer(ResourcePlayerPool pool, Cellar.Resource resource) {
            base(pool, resource);

            this.end_mode = this.resource.get_field("end-mode", "cut");

            this.pipeline = new Gst.Pipeline(null);

            this.videoconvert = Gst.ElementFactory.make("videoconvert", null);
            this.audioconvert = Gst.ElementFactory.make("audioconvert", null);

            this.volume = Gst.ElementFactory.make("volume", null);
            Photon.Config.get_instance().notify["volume"].connect(this.update_volume);
            Photon.Config.get_instance().notify["volume-mute"].connect(this.update_volume);
            DisplayManager.get_instance().notify["hidden"].connect(this.update_volume);
            this.update_volume();

            this.audiosink = Gst.ElementFactory.make("autoaudiosink", null);

            this.uridecodebin = Gst.ElementFactory.make("uridecodebin", null);
            this.uridecodebin.pad_added.connect((src, new_pad) => {
                if (new_pad.is_linked()) return;

                Gst.Caps caps = new_pad.get_current_caps();
                unowned Gst.Structure struct = caps.get_structure(0);
                string pad_type = struct.get_name();
                if (pad_type.has_prefix("video/x-raw")) {
                    Gst.Pad sink = this.videoconvert.get_static_pad("sink");
                    new_pad.link(sink);
                    Gst.Debug.BIN_TO_DOT_FILE(this.pipeline, ALL, "pipeline");
                } else if (pad_type.has_prefix("audio/x-raw")) {
                    this.pipeline.add_many(this.audioconvert, this.volume, this.audiosink);
                    Gst.Pad sink = this.audioconvert.get_static_pad("sink");
                    new_pad.link(sink);
                    this.audioconvert.link(this.volume);
                    this.volume.link(this.audiosink);
                    this.audioconvert.sync_state_with_parent();
                    this.volume.sync_state_with_parent();
                    this.audiosink.sync_state_with_parent();
                    Gst.Debug.BIN_TO_DOT_FILE(this.pipeline, ALL, "pipeline");
                }
            });
            this.uridecodebin.uri = resource.file.get_uri();

            this.appsink = (Gst.App.Sink) Gst.ElementFactory.make("appsink", null);
            this.appsink.emit_signals = true;
            this.appsink.caps = new Gst.Caps.simple(
                "video/x-raw",
                "format", Type.STRING, BYTE_ORDER == LITTLE_ENDIAN ? "BGRA" : "RGBA"
            );
            this.appsink.new_sample.connect(this.on_new_sample);

            this.pipeline.add_many(this.uridecodebin, this.videoconvert, this.appsink);
            this.videoconvert.link(this.appsink);

            if (this.pipeline == null || this.videoconvert == null
                || this.audioconvert == null || this.volume == null
                || this.audiosink == null || this.uridecodebin == null
                || this.appsink == null) {
                warning("An element of the ResourcePlayer pipeline is NULL! It will not work properly!");
            }

            this.start_pipeline();
        }


        public override void destroy() {
            this.pipeline.set_state(Gst.State.NULL);
            this.bus.remove_signal_watch();
        }

        public override void set_paused(bool paused) {
            this.pipeline.set_state(paused ? Gst.State.PAUSED : Gst.State.PLAYING);
        }

        public override double get_position() {
            int64 result = -1;
            this.pipeline.query_position(TIME, out result);
            return result / 1000000000.0;
        }

        public override void seek(double position) {
            if (position < this.duration) {
                int64 nano = (int64) (position * 1000000000);
                this.pipeline.seek_simple(Gst.Format.TIME, FLUSH, nano);
            } else {
                switch (this.end_mode) {
                    case "loop":
                        position = position % this.duration;
                        int64 nano = (int64) (position * 1000000000);
                        this.pipeline.seek_simple(Gst.Format.TIME, FLUSH, nano);
                        break;
                    case "cut":
                        int64 nano = (int64) (position * 1000000000);
                        this.pipeline.seek_simple(Gst.Format.TIME, FLUSH, nano);
                        this.buffer.clear();
                        break;
                    case "freeze":
                        // seek to 1ms before the very end and let it stay there
                        int64 nano = (int64) (this.duration * 1000000000);
                        this.pipeline.seek_simple(Gst.Format.TIME, FLUSH, nano - 1000);
                        break;
                }
            }
        }


        private Gst.FlowReturn on_new_sample(Gst.App.Sink sink) {
            Gst.Sample sample = this.appsink.pull_sample();
            if (sample == null) return Gst.FlowReturn.EOS;

            if (!this.buffer_initialized) {
                unowned Gst.Structure s = sample.get_caps().get_structure(0);
                int width, height;
                s.get_int("width", out width);
                s.get_int("height", out height);
                this.buffer = new DoubleBuffer(width, height);
                this.buffer_initialized = true;
            }

            Gst.Buffer buf = sample.get_buffer();
            this.buffer.with_back((back) => {
                Photon.copy_buffer_to_surface(buf, back);
            });
            this.buffer.swap();

            return Gst.FlowReturn.OK;
        }

        private void update_volume() {
            this.volume.mute = this.mute ||
                               Photon.Config.get_instance().volume_mute ||
                               Halogen.DisplayManager.get_instance().hidden;

            double resource_volume = double.parse(this.resource.get_field("volume", "100"));
            double main_volume = Photon.Config.get_instance().volume;
            this.volume.volume = resource_volume * 0.01 * main_volume * 0.01;
            debug("volume %f (%f %f) %s", this.volume.volume, resource_volume, main_volume, this.volume.mute ? " muted" : "");
        }

        private void start_pipeline() {
            // Let the pool know we're playing something and we'll call
            // player_stop() when we're done
            this.pool.player_start();

            this.pipeline.set_state(Gst.State.PLAYING);

            this.bus = pipeline.get_bus();
            this.bus.add_signal_watch();
            this.bus.message.connect((msg) => {
                switch (msg.type) {
                    case Gst.MessageType.ERROR:
                        Error err;
                        string dbg;
                        msg.parse_error(out err, out dbg);
                        warning("Received error from pipeline! %s", err.message);
                        debug("Debug info for error: %s", dbg);
                        break;
                    case Gst.MessageType.ASYNC_DONE:
                        int64 duration;
                        this.pipeline.query_duration(TIME, out duration);
                        this.duration = (duration / 1000000000.0);
                        break;
                    case Gst.MessageType.EOS:
                        // Yes, this should be called even if the resource
                        // loops. See ResourcePlayerPool::finished.
                        this.pool.player_stop();

                        switch (this.end_mode) {
                            case "loop":
                                this.pipeline.seek_simple(Gst.Format.TIME, FLUSH, 0);
                                break;

                            case "cut":
                                this.buffer.clear();
                                break;

                            case "freeze":
                            default:
                                // do nothing
                                break;
                        }
                        break;
                }
            });
        }
    }
}
