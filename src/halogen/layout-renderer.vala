namespace Halogen {
    /**
     * Renders a LayoutPage for a specific slide.
     */
    public class LayoutRenderer : Object {
        /**
         * The page that this LayoutRenderer is based on.
         */
        public LayoutPage page;


        private static ResourcePlayerPool thumbnail_pool;
        public static ResourcePlayerPool get_thumbnail_pool() {
            if (thumbnail_pool == null) thumbnail_pool = new ResourcePlayerPool();
            return thumbnail_pool;
        }
        private static ResourcePlayerPool background_pool;
        public static ResourcePlayerPool get_background_pool() {
            if (background_pool == null) background_pool = new ResourcePlayerPool();
            return background_pool;
        }


        /**
         * The ResourcePlayer for the slide's background.
         */
        private ResourcePlayer? background;

        /**
         * The DrawOpts to use when drawing the background.
         */
        private DrawOpts? background_opts;

        /**
         * The WidgetInstance objects to render
         */
        private Gee.List<WidgetInstance> widgets;

        /**
         * Whether the LayoutRenderer is being used to generate thumbnails
         * rather than a live view.
         */
        private bool thumbnail = false;


        internal LayoutRenderer(LayoutPage page,
                                Cellar.Segment? segment,
                                Cellar.Slide slide,
                                bool thumbnail) {
            this.thumbnail = thumbnail;
            this.page = page;

            this.widgets = new Gee.ArrayList<WidgetInstance>();

            if (segment != null && segment.background != null && !segment.background.deleted) {
                ResourcePlayerPool pool = thumbnail
                    ? LayoutRenderer.get_thumbnail_pool()
                    : LayoutRenderer.get_background_pool();
                this.background = pool.get_player(segment.background, thumbnail);
                this.background_opts = new DrawOpts.from_resource(segment.background);
            }

            foreach (LayoutWidget widget in page.widgets) {
                this.widgets.add(new WidgetInstance(widget, slide, this.thumbnail));
            }
        }

        ~LayoutRenderer() {
            if (this.background != null) this.background.return_to_pool();
        }


        /**
         * Renders the layout onto the cairo surface in the given rectangle.
         */
        public void render(Cairo.Context cr, int x, int y, int w, int h) {
            cr.save();

            cr.rectangle(x, y, w, h);
            cr.clip_preserve();
            cr.set_source_rgb(0, 0, 0);
            cr.fill();

            // Draw background, if there is one
            if (this.background != null) {
                this.background.buffer.with_front((front) => {
                    draw_to_context(
                        cr, front,
                        x, y, w, h,
                        this.background_opts
                    );
                });
            }

            foreach (WidgetInstance widget in this.widgets) {
                widget.render(cr, x, y, w, h);
            }

            cr.restore();
        }
    }


    /**
     * A widget that is specific to a LayoutRenderer. Contains
     * information about the content of the widget as well as a reference to
     * the LayoutWidget it is based on.
     */
    private class WidgetInstance {
        /**
         * The widget to draw
         */
        public LayoutWidget widget;

        /**
         * The ResourcePlayer playing the media, if a media widget.
         */
        public ResourcePlayer player;

        /**
         * For media widgets, the DrawOpts to use when drawing the image.
         */
        public DrawOpts draw_opts;

        /**
         * The text to draw, if a text widget.
         */
        public string text;


        public WidgetInstance(LayoutWidget widget, Cellar.Slide slide, bool thumbnail) {
            this.widget = widget;

            string? field = slide.get_field(widget.field);
            if (field == null) return;

            switch (widget.widget_type) {
                case TEXT:
                    this.text = field;
                    break;
                case MEDIA:
                    var library = Cellar.Library.get_instance();
                    Cellar.Resource resource = library.get_resource(field);

                    // if the resource doesn't exist anymore, the widget is a no-op
                    if (resource == null) return;

                    this.draw_opts = new DrawOpts.from_resource(resource);

                    ResourcePlayerPool pool = thumbnail
                        ? LayoutRenderer.get_thumbnail_pool()
                        : State.get_instance().pool;
                    this.player = pool.get_player(resource, thumbnail);

                    if (!widget.page.layout.get_field(widget.field).muted) {
                        this.player.mute = false;
                    }
                    break;
            }
        }

        ~WidgetInstance() {
            if (this.player != null) this.player.return_to_pool();
        }


        /**
         * Renders the widget.
         */
        public void render(Cairo.Context cr, int x, int y, int w, int h) {
            if (this.text != null) {
                render_text(
                    this.text, cr, widget.text_opts,
                    x + w * widget.x, y + h * widget.y, w * widget.width, h * widget.height,
                    h
                );
            } else if (this.player != null) {
                this.player.buffer.with_front((front) => {
                    draw_to_context(
                        cr, front,
                        x + w * widget.x, y + h * widget.y, w * widget.width, h * widget.height,
                        this.draw_opts
                    );
                });
            }
        }
    }
}
