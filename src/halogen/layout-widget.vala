using Gee;

namespace Halogen {
    public enum LayoutWidgetType {
        /**
         * Displays text, possibly with formatting.
         */
        TEXT,

        /**
         * Displays a media resource.
         */
        MEDIA
    }

    /**
     * A widget in a layout, for displaying something.
     */
    public class LayoutWidget : Object {
        /**
         * The LayoutPage this LayoutWidget is on.
         */
        public weak LayoutPage page { get; private set; }

        /*
         * All coordinates are doubles from 0 to 1, with 0 being top/left and 1
         * being bottom/right.
         */
        public double x { get; private set; }
        public double y { get; private set; }
        public double width { get; private set; }
        public double height { get; private set; }

        public LayoutWidgetType widget_type { get; private set; }

        public TextOptions? text_opts { get; private set; }

        /**
         * The slide field from which to get the text (for text widgets) or
         * media resource UUID (for media widgets).
         */
        public string field { get; private set; }


        private Json.Object json;


        internal LayoutWidget(LayoutPage page, Json.Object json) {
            this.page = page;
            this.json = json;

            this.x = this.json.get_double_member("x");
            this.y = this.json.get_double_member("y");
            this.width = this.json.get_double_member("width");
            this.height = this.json.get_double_member("height");

            switch (this.json.get_string_member("type")) {
                case "text":
                default:
                    this.widget_type = LayoutWidgetType.TEXT;
                    break;
                case "media":
                    this.widget_type = LayoutWidgetType.MEDIA;
                    break;
            }

            if (this.json.has_member("field")) {
                this.field = this.json.get_string_member("field");
            }

            if (this.json.has_member("text-options")) {
                this.text_opts = new TextOptions.from_json(
                    this.json.get_object_member("text-options")
                );
            }
        }
    }
}
