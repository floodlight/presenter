namespace Halogen {
    public class DisplayWindow : Gtk.Window {
        public string? feed_id {
            get {
                if (this.feed == null) return null;
                return this.feed.id;
            }
            set {
            lock (this.feed) {
                if (value == null) {
                    this.feed = null;
                } else {
                    this.feed = FeedManager.get_instance().create_feed(value);
                    this.feed.resize(this.buffer.width, this.buffer.height);
                }
            }
            }
        }

        /**
         * The double buffer used for drawing
         */
        public DoubleBuffer buffer;


        private Gtk.DrawingArea da;

        private Feed feed;

        /**
         * The window's rendering thread
         */
        private Thread thread;

        /**
         * The index of the monitor this display window should be on.
         */
        private int index;

        private Json.Object json;


        public DisplayWindow(int index, Json.Object json) {
            this.index = index;
            this.json = json;

            this.accept_focus = false;

            this.buffer = new DoubleBuffer(1, 1);

            this.feed_id = json.get_string_member("feed");

            this.da = new Gtk.DrawingArea();
            this.add(this.da);
            this.da.draw.connect((cr) => {
                cr.set_source_rgb(0, 0, 0);
                cr.paint();

                this.buffer.with_front((front) => {
                    cr.set_source_surface(front, 0, 0);
                    cr.paint();
                });
                return false;
            });

            this.da.size_allocate.connect((alloc) => {
                this.buffer.resize(alloc.width, alloc.height);
                lock (this.feed) {
                    if (this.feed != null) {
                        this.feed.resize(alloc.width, alloc.height);
                    }
                }
            });

            this.thread = new Thread<int>(null, this.thread_func);

            this.da.show();
        }


        public void show_window() {
            var monitor = Gdk.Display.get_default().get_monitor(this.index);
            this.move(monitor.geometry.x, monitor.geometry.y);
            this.fullscreen_on_monitor(this.screen, this.index);
            base.show();
        }

        public void close_window() {
            // Don't close the window while rendering, that's how you get
            // segfaults
            lock (this.feed) {
                this.close();
            }
        }


        /**
         * Function for the rendering thread
         */
        private int thread_func() {
            while (true) {
                lock(this.feed) {
                    if (this.feed != null) {
                        this.buffer.with_back((back) => {
                            Cairo.Context cr = new Cairo.Context(back);
                            this.feed.render(cr, 0, 0);
                        });
                        this.buffer.swap();
                    }
                }

                Idle.add(() => {
                    this.da.queue_draw();
                    return Source.REMOVE;
                });

                Thread.usleep(1000000 / Photon.Config.get_instance().framerate);
            }
        }
    }
}
