using Gee;

namespace Halogen {
    /**
     * Describes how a slide should be displayed on different feeds.
     */
    public class Layout : Object {
        /**
         * The display name of the layout.
         */
        public string name { get; private set; }

        /**
         * The internal ID of the layout.
         */
        public string id { get; private set; }


        /**
         * Map of pages in the layout by their internal IDs.
         *
         * Those IDs are not UUIDs. They are only used internally by the
         * layout.
         */
        private HashMap<string, LayoutPage> pages;

        /**
         * The slide fields that are used in the layout.
         */
        private ArrayList<LayoutField> fields;

        private Json.Object json;

        /**
         * The entry in the index
         */
        private Json.Object entry;


        /**
         * Creates a new Layout object.
         * @entry: The JSON object for the layout's index entry. Contains info
         * specific to this installation of Presenter.
         */
        internal Layout(string id, Json.Object entry) {
            this.id = id;
            this.entry = entry;

            this.pages = new HashMap<string, LayoutPage>();
            this.fields = new ArrayList<LayoutField>();

            try {
                var parser = new Json.Parser();
                string resource = "resource:///io/gitlab/floodlight/Presenter/layouts/%s.json".printf(id);
                File file = File.new_for_uri(resource);
                parser.load_from_stream(file.read());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                critical("LAYOUT DOES NOT EXIST: %s (ERROR: %s)", id, e.message);
                return;
            }

            this.name = this.json.get_string_member("name");

            this.json.get_array_member("fields").foreach_element(
                (obj, index, val) => {
                    this.fields.add(new LayoutField(val.get_object()));
                }
            );

            this.json.get_object_member("pages").foreach_member(
                (obj, key, val) => {
                    this.pages.set(key, new LayoutPage(this, val.get_object()));
                }
            );
        }


        /**
         * Returns whether the layout has a field with the given ID.
         */
        public bool has_field(string id) {
            foreach (LayoutField field in this.fields) {
                if (field.id == id) return true;
            }
            return false;
        }

        /**
         * Gets a field by ID.
         */
        public LayoutField? get_field(string id) {
            foreach (LayoutField field in this.fields) {
                if (field.id == id) return field;
            }
            warning("Requested field %s which does not exist! Use Layout.has_field() first", id);
            return null;
        }

        /**
         * Gets a list of all fields in the layout.
         */
        public Gee.Collection<LayoutField> get_field_list() {
            return this.fields;
        }

        /**
         * Gets the LayoutPage for the given feed, or the default page if
         * the feed can't be found.
         */
        public LayoutPage page_for_feed(string feed_id) {
            Json.Object feeds = this.entry.get_object_member("feeds");
            string page_name;
            if (feeds.has_member(feed_id)) {
                page_name = feeds.get_string_member(feed_id);
            } else {
                page_name = this.json.get_string_member("default-page");
            }

            return this.pages.get(page_name);
        }
    }


    public enum LayoutFieldType {
        /**
         * A resource UUID
         */
        RESOURCE,

        /**
         * A user-defined string
         */
        TEXT,

        /**
         * A user-defined string. User interfaces should use a multiline text
         * input for this field.
         */
        MULTILINE
    }

    /**
     * A content field used by the layout. Slides should provide these fields.
     */
    public class LayoutField {
        /**
         * The display name of the field.
         */
        public string name { get; private set; }

        public string id { get; private set; }

        /**
         * The type of field. This affects how it is chosen by the user
         * (though the actual data is always a string).
         */
        public LayoutFieldType field_type { get; private set; }

        /**
         * If the field is a resource, whether the resource player should not
         * play sound.
         */
        public bool muted { get; private set; }


        internal LayoutField(Json.Object entry) {
            this.id = entry.get_string_member("id");
            this.name = entry.get_string_member("name");

            if (entry.has_member("muted")) {
                this.muted = entry.get_boolean_member("muted");
            }

            switch (entry.get_string_member("type")) {
                case "resource":
                    this.field_type = LayoutFieldType.RESOURCE;
                    break;
                case "text":
                    this.field_type = LayoutFieldType.TEXT;
                    break;
                default:
                case "multiline":
                    this.field_type = LayoutFieldType.MULTILINE;
                    break;
            }
        }
    }
}
