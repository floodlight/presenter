namespace Halogen {
    public class DisplayManager : Object {
        /**
         * Emitted when the physical displays plugged in to the computer are
         * changed.
         */
        public signal void physical_displays_changed();

        /**
         * Emitted when the settings for any display are changed.
         */
        public signal void changed();


        public static DisplayManager instance;
        public static DisplayManager get_instance() {
            if (instance == null) instance = new DisplayManager();
            return instance;
        }


        private Gee.HashMap<int, Display> displays;

        private File file;
        private Json.Object json;


        /**
         * Whether the windows may be shown. Use show() and hide() to change.
         */
        public bool hidden { get; private set; default = true; }


        private DisplayManager() {
            this.displays = new Gee.HashMap<int, Display>();

            var config = Photon.Config.get_instance();
            this.file = File.new_for_path(config.datadir)
                        .get_child("config")
                        .get_child("displays.json");

            try {
                var parser = new Json.Parser();
                parser.load_from_file(this.file.get_path());
                this.json = parser.get_root().get_object();
            } catch (Error e) {
                debug("No display config found, starting anew");
                var parser = new Json.Parser();

                try {
                    File defaults = File.new_for_uri("resource:///io/gitlab/floodlight/Presenter/defaults/display-config.json");
                    parser.load_from_stream(defaults.read());
                    this.json = parser.get_root().get_object();
                } catch (Error e) {
                    critical("ERROR! COULD NOT CREATE INITIAL DISPLAY CONFIG: %s", e.message);
                }
            }

            this.json.get_object_member("displays").foreach_member(
                (json, key, val) => {
                    int index = int.parse(key);
                    this.displays[index] = new Display(index, val.get_object());
                }
            );

            this.debug_dump();
            Gdk.Screen.get_default().monitors_changed.connect(() => {
                this.debug_dump();
                this.physical_displays_changed();
                this.changed();
                this.show();
            });
        }


        /**
         * Gets the number of physical displays plugged into the computer.
         */
        public int get_physical_display_count() {
            return Gdk.Display.get_default().get_n_monitors();
        }

        /**
         * Gets a Display object by index, creating it if necessary.
         */
        public Display get_display(int index) {
            Display? display = this.displays.get(index);
            if (display == null) {
                display = new Display.from_scratch(index);
                this.displays[index] = display;
            }
            return display;
        }

        /**
         * Shows all DisplayWindows that are enabled and whose displays are
         * connected.
         */
        public void show() {
            foreach (Display display in this.displays.values) {
                display.show_window();
            }
            this.hidden = false;
        }

        /**
         * Hides all DisplayWindows.
         */
        public void hide() {
            foreach (Display display in this.displays.values) {
                display.hide_window();
            }
            this.hidden = true;
        }

        /**
         * Saves the display configuration.
         */
        public void save() {
            Photon.save_json(this.file, this.json);
        }


        /**
         * Prints information about the physical displays connected to the
         * computer.
         */
        private void debug_dump() {
            int monitors = this.get_physical_display_count();

            for (int i = 0; i < monitors; i ++) {
                var monitor = Gdk.Display.get_default().get_monitor(i);
                Gdk.Rectangle geometry = monitor.geometry;
                debug("Monitor %d: (%d %d %d %d)",
                    i, geometry.x, geometry.y, geometry.width, geometry.height
                );
            }
        }
    }


    /**
     * Represents a single display that is known to Floodlight. It might not be
     * enabled or have a window.
     */
    public class Display : Object {
        /**
         * The DisplayWindow for the display, if it has one.
         */
        public DisplayWindow? window { get; private set; }

        /**
         * Whether the display is enabled.
         *
         * You cannot set this directly. Setting a feed name will automatically
         * enable the display.
         */
        public bool enabled {
            get {
                return this.feed_id != null;
            }
        }

        private string? _feed_id;
        /**
         * The name of the feed that the display should use. Set to null to
         * disable the display.
         */
        public string? feed_id {
            get {
                return _feed_id;
            }
            set {
                this._feed_id = value;

                this.json.set_string_member("feed", value);

                if (this.window != null) {
                    this.window.feed_id = value;
                }

                if (this._feed_id == null) {
                    this.close_window();
                } else {
                    this.show_window();
                }

                DisplayManager.get_instance().changed();
                DisplayManager.get_instance().save();
            }
        }


        private Json.Object json;
        private int index;

        /**
         * Creates a new Display with the given index and JSON data.
         */
        public Display(int index, Json.Object json) {
            this.index = index;
            this.json = json;

            this._feed_id = this.json.get_string_member("feed");
        }

        /**
         * Creates a new Display with the given index. It will not be enabled.
         */
        public Display.from_scratch(int index) {
            var json = new Json.Object();
            json.set_null_member("feed");
            this(index, json);
        }


        /**
         * Shows the display's window, if it is enabled and the physical display
         * is plugged in. Creates a window if necessary.
         *
         * Will actually close the window if it is open but the display is not
         * plugged in.
         */
        public void show_window() {
            if (DisplayManager.get_instance().hidden) return;

            int displays = DisplayManager.get_instance().get_physical_display_count();
            if (this.index >= displays || !this.enabled) {
                this.close_window();
                return;
            }

            if (this.window == null) {
                this.window = new DisplayWindow(this.index, this.json);
                DisplayManager.get_instance().changed();
            }

            this.window.show_window();
        }

        public void hide_window() {
            if (this.window != null) {
                this.window.hide();
            }
        }


        /**
         * Closes the window, if it exists.
         */
        private void close_window() {
            if (this.window != null) {
                this.window.close_window();
                DisplayManager.get_instance().changed();
                this.window = null;
            }
        }
    }
}
