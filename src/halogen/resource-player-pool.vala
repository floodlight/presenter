namespace Halogen {
    /**
     * Loads and keeps track of #ResourcePlayer objects. Used, for example, for
     * backgrounds.
     */
    public class ResourcePlayerPool : Object {
        /**
         * Emitted when all players in the pool have played through at least
         * once.
         *
         * This is used to advance the slide when everything finishes, for
         * slides that have that option set.
         *
         * Note that if a resource loops, this will be called after the loop
         * has run once. This is by design. If you have a resource that loops
         * and you do not like this behavior, you should not set the slide to
         * continue automatically.
         */
        public signal void finished();


        /**
         * The max duration of all the resources in the pool.
         */
        public double duration { get; private set; }

        private double _fixed_duration = 0;
        /**
         * A fixed duration to use rather than the actual duration of the
         * videos. This is used when a slide's continue_mode is set to TIMER.
         */
        public double fixed_duration {
            get {
                return _fixed_duration;
            }
            set {
                this._fixed_duration = value;
                this.duration = this.fixed_duration;
                this.start_position = 0;
                this.update_fixed_duration_timer();
            }
        }

        /**
         * Whether the pool is paused
         */
        public bool paused { get; private set; }


        /**
         * Map of resource players by the IDs of the resources they are playing.
         * playing.
         */
        private Gee.HashMap<string, ResourcePlayerPoolItem?> players;

        /**
         * Number of players that have not ended yet.
         *
         * This is incremented by a ResourcePlayer calling player_start()
         * and decremented by a player calling player_stop(). When this drops
         * back to 0, `finished` will be called.
         */
        private int playing;

        /**
         * The time play was last started, according to get_monotonic_time(),
         * if in fixed duration mode.
         */
        private int64 start_time;

        /**
         * The position of the player when start_time was last set.
         */
        private double start_position;

        /**
         * The ID of the fixed-duration timer.
         */
        private uint timer_id;


        public ResourcePlayerPool() {
            this.players = new Gee.HashMap<string, ResourcePlayerPoolItem?>();
        }


        /**
         * Gets a #ResourcePlayer for the resource, loading it if necessary.
         *
         * You must call return_player() when you are finished with the player.
         */
        public ResourcePlayer get_player(Cellar.Resource resource, bool thumbnail = false) {
        lock (this.players) {
            if (!this.players.has_key(resource.uuid)) {
                ResourcePlayer player = ResourcePlayer.get_player(this, resource, thumbnail);
                ResourcePlayerPoolItem item = new ResourcePlayerPoolItem() {
                    player = player,
                    refcount = 0
                };
                this.players.set(resource.uuid, item);

                player.notify["duration"].connect(this.update_duration);
            }

            ResourcePlayerPoolItem item = this.players.get(resource.uuid);
            item.refcount ++;
            return item.player;
        }
        }

        /**
         * Returns the #ResourcePlayer created for the given resource.
         */
        public void return_player(Cellar.Resource resource) {
        lock (this.players) {
            ResourcePlayerPoolItem item = this.players.get(resource.uuid);
            item.refcount --;
            if (item.refcount <= 0) {
                item.player.destroy();
                this.players.unset(resource.uuid);
            }
        }
        }

        /**
         * Pauses or unpauses all players in the pool
         */
        public void pause(bool paused) {
            var was_paused = this.paused;
            foreach (ResourcePlayerPoolItem item in this.players.values) {
                item.player.set_paused(paused);
            }
            this.paused = paused;

            if (!was_paused && paused) {
                this.start_position += (get_monotonic_time() - this.start_time) / Photon.MICROSECOND;
            }
            this.update_fixed_duration_timer();
        }

        /**
         * Gets the position of the players in the pool, or -1 if not applicable
         */
        public double get_position() {
            if (this.fixed_duration > 0) {
                if (this.paused) return this.start_position;
                else return ((get_monotonic_time() - this.start_time) / Photon.MICROSECOND) + this.start_position;
            }

            // The positions may be different if some of the resources loop.
            // Get the position of the longest one.

            double longest = 0;
            double result = -1;

            foreach (ResourcePlayerPoolItem item in this.players.values) {
                ResourcePlayer player = item.player;
                if (player is VideoPlayer) {
                    if (((VideoPlayer) player).duration > longest) {
                        result = item.player.get_position();
                    }
                }
            }

            return result;
        }

        /**
         * Seeks all players to the given position.
         */
        public void seek(double position) {
            if (this.fixed_duration > 0) {
                this.start_position = position;
                update_fixed_duration_timer();
            }

            foreach (ResourcePlayerPoolItem item in this.players.values) {
                item.player.seek(position);
            }
        }

        /**
         * Increases the internal counter of players that are playing.
         * See `playing`.
         */
        public void player_start() {
            this.playing ++;
        }

        /**
         * Decreases the internal counter of players that are playing.
         * See `playing`.
         */
        public void player_stop() {
            this.playing --;
            if (playing == 0) {
                this.finished();
            }
        }


        /**
         * Gets the duration of the resource player pool, in seconds. This is
         * the maximum duration of the individual resource players.
         *
         * If there are no videos in the pool, 0 will be returned.
         */
        private void update_duration() {
            if (this.fixed_duration > 0) {
                this.duration = this.fixed_duration;
                return;
            }

            double duration = 0;
            foreach (ResourcePlayerPoolItem item in this.players.values) {
                ResourcePlayer player = item.player;
                if (player is VideoPlayer) {
                    duration = double.max(((VideoPlayer) player).duration, duration);
                }
            }
            this.duration = duration;
        }

        private void update_fixed_duration_timer() {
            if (this.fixed_duration <= 0) return;

            if (this.timer_id > 0) {
                Source.remove(this.timer_id);
                this.timer_id = 0;
            }

            if (!this.paused) {
                this.start_time = get_monotonic_time();
                int time = (int) ((this.fixed_duration - this.start_position) * 1000.0);
                this.timer_id = Timeout.add(time, this.on_timer_end);
            }
        }

        private bool on_timer_end() {
            this.finished();
            this.timer_id = 0;
            return Source.REMOVE;
        }
    }


    /**
     * Item in a ResourcePlayerPool, containing the ResourcePlayer and its
     * refcount.
     */
    internal class ResourcePlayerPoolItem {
        public ResourcePlayer player;
        public int refcount;
    }
}
