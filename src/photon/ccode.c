#include <glib.h>
#include <cairo.h>
#include <gst/gst.h>

void
photon_copy_buffer_to_surface (GstBuffer *buf, cairo_surface_t *surface)
{
  guint length = cairo_image_surface_get_height (surface) * cairo_image_surface_get_stride (surface);
  guint8 *data = cairo_image_surface_get_data (surface);
  gst_buffer_extract (buf, 0, data, length);
  cairo_surface_mark_dirty (surface);
}
