namespace Photon {
    public const double MICROSECOND = 1000 * 1000;

    private Gee.HashMap<File, Json.Object> files_to_save = null;
    private bool save_queued = false;

    /**
     * Saves a JSON object to a file asynchronously.
     */
    public void save_json(File file, Json.Object obj) {
        if (files_to_save == null) {
            files_to_save = new Gee.HashMap<File, Json.Object>(
                (file) => file.hash(),
                (a, b) => a.equal(b)
            );
        }

        files_to_save[file] = obj;

        if (!save_queued) {
            Timeout.add(1000, () => {
                save_files.begin();
                return Source.REMOVE;
            });
            save_queued = true;
        }
    }

    /**
     * Emits a critical error when run on a thread that is not the main GTK
     * thread. This is used to ensure that certain functions do not run on the
     * wrong thread.
     *
     * It may be disabled in release builds.
     */
    public void ensure_main_thread() {
        if (!MainContext.@default().is_owner()) {
            critical("MULTITHREADING ERROR DETECTED: ensure_main_thread() was not called on the main thread. This is a good indication there is a VERY NASTY BUG waiting to manifest itself at an inopportune time!");
        }
    }

    /**
     * Saves the JSON object without waiting for the regular timeout. This
     * should usually not be done, since it could cause out-of-order writes.
     * Currently, it is only used when importing songs.
     */
    public async void save_json_now(File file, Json.Object obj) {
        Json.Node root = new Json.Node(Json.NodeType.OBJECT);
        root.set_object(obj);

        var generator = new Json.Generator();
        generator.set_root(root);
        // pretty print when in debug mode
        if (Config.get_instance().debugmode) generator.pretty = true;

        try {
            file.get_parent().make_directory_with_parents();
        } catch (Error e) {
            // ignore, the directory already exists
        }

        string text = generator.to_data(null);

        // log file io for debugging purposes
        string? path = File.new_for_path(Config.get_instance().datadir).get_relative_path(file);
        debug("%7dB %s", text.length, path);

        try {
            FileOutputStream stream = yield file.replace_async(null, false, PRIVATE, Priority.DEFAULT, null);
            yield stream.write_all_async(text.data, Priority.DEFAULT, null, null);
            yield stream.close_async();
        } catch (Error e) {
            warning("Failed to write JSON file: %s", e.message);
            warning("file: %s", path);
        }
    }

    private async void save_files() {
        Gee.Map.Entry<File, Json.Object>[] entries = files_to_save.entries.to_array();

        foreach (var entry in entries) {
            File file = entry.key;
            Json.Object obj = entry.@value;
            yield save_json_now(file, obj);
            files_to_save.unset(file);
        }

        save_queued = false;
    }


    /**
     * Converts a number of seconds into a human-friendly string, like 3:04
     * (for 184 seconds).
     */
    private string seconds_to_string(int s) {
        int hours = s / 3600;
        int minutes = (s % 3600) / 60;
        int seconds = s % 60;

        // \u2236 is the "ratio" symbol
        if (hours > 0) {
            return "%d\u2236%02d\u2236%02d".printf(hours, minutes, seconds);
        } else {
            return "%d\u2236%02d".printf(minutes, seconds);
        }
    }

    /**
     * Creates a deep copy of a JSON object.
     */
    public Json.Object duplicate_json(Json.Object obj) {
        // Do this the quick-and-dirty way: serialize it, then deserialize it
        var gen = new Json.Generator();
        var node = new Json.Node.alloc();
        node.init_object(obj);
        gen.root = node;
        string data = gen.to_data(null);

        var parser = new Json.Parser();
        try {
            parser.load_from_data(data);
        } catch (Error e) {
            critical("COULD NOT DUPLICATE JSON OBJECT!");
            debug("Problematic JSON: %s", data);
        }
        return parser.get_root().get_object();
    }
}
