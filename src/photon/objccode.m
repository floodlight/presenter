#include <Cocoa/Cocoa.h>
#include <glib-object.h>

/**
 * photon_macos_get_data_dir:
 * Gets the directory where user application data should be stored (usually
 * ~/Library/Application Support [why does the filename have to have spaces?])
 */
char *
photon_macos_get_data_dir ()
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains (
    NSApplicationSupportDirectory,
    NSUserDomainMask,
    YES
  );

  NSString *path = [paths firstObject];
  if (path == nil) return NULL;

  return g_strdup ([path UTF8String]);
}
