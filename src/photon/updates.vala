namespace Photon {
    public class UpdateCheck : Object {
        public const string NEWS_FEED_URL = "https://floodlight.gitlab.io/presenter/NEWS";
        public const string DOWNLOADS_URL = "https://floodlight.gitlab.io/download";


        private static UpdateCheck instance;
        public static UpdateCheck get_instance() {
            if (instance == null) instance = new UpdateCheck();
            return instance;
        }


        /**
         * Whether an update is known to be available.
         *
         * This might change when the HTTP request finishes or if the
         * "check_for_updates" config variable changes.
         */
        public bool update_available { get; private set; default=false; }

        /**
         * The changelog.
         *
         * This contains the entire changelog, all the way back to the
         * beginning. If you just want changes since the current version, use
         * get_newer_versions().
         */
        public Gee.List<ChangelogEntry> changelog { get; private set; }


        private UpdateCheck() {
            this.changelog = new Gee.ArrayList<ChangelogEntry>();

            Config.get_instance().notify["check-for-updates"].connect((s, p) => {
                if (Config.get_instance().check_for_updates) {
                    this.start_check();
                } else {
                    this.update_available = false;
                }
            });

            this.start_check();
        }


        /**
         * Gets all versions newer than the current one, from latest to
         * earliest.
         */
        public async Gee.List<ChangelogEntry> get_newer_versions() {
            var result = new Gee.ArrayList<ChangelogEntry>();
            var current = BUILD_VERSION;

            if (this.changelog == null) {
                // return an empty list, there are no newer versions
                return result;
            }

            foreach (var entry in this.changelog) {
                if (entry.version == current) break;
                result.add(entry);
            }

            return result;
        }


        /**
         * Starts the update check.
         */
        private void start_check() {
            if (!Config.get_instance().check_for_updates) return;

            var session = new Soup.Session();
            var message = new Soup.Message("GET", NEWS_FEED_URL);
            session.send_and_read_async.begin(message, Soup.MessagePriority.NORMAL, null, (sess, result) => {
                var bytes = ((Soup.Session)sess).send_and_read_async.end(result);
                if (message.status_code != 200) {
                    // don't do anything beyond emitting a warning
                    warning("Checking for updates failed: %s", message.reason_phrase);
                    return;
                }

                this.parse_news((string) bytes.get_data());
            });
        }

        /**
         * Parses the NEWS file into a list of entries, then detects if any
         * of them are newer than the current version. If so, sets
         * :update_available accordingly. Also updates :changelog.
         */
        private void parse_news(string news) {
            this.changelog.clear();

            string[] lines = news.split("\n");

            ChangelogEntry current = null;
            bool fields_section = false;
            for (int i = 0; i < lines.length; i ++) {
                string line = lines[i];
                string? next = i + 1 < lines.length ? lines[i + 1] : null;

                if (i == 0 || next != null && /^=+$/.match(next)) {
                    if (current != null) {
                        current.finish();
                        this.changelog.add(current);
                    }
                    current = new ChangelogEntry(line);

                    // now starts the fields section of the entry
                    fields_section = true;

                    // skip equal signs and mandatory blank line
                    i += 2;
                } else if (fields_section) {
                    MatchInfo match;
                    (/([\w\d\- ]+):\s+(.+)/).match(line, 0, out match);
                    if (!match.matches()) {
                        // fields section is over
                        fields_section = false;
                    } else {
                        current.set_field(match.fetch(1), match.fetch(2));
                    }
                } else {
                    current.add_line(line);
                }
            }

            if (current != null) {
                current.finish();
                this.changelog.add(current);
            }

            this.update_available = this.changelog.size > 0
                                    && this.changelog[0].version != BUILD_VERSION;
        }
    }


    /**
     * Represents an entry in the NEWS file.
     */
    public class ChangelogEntry : Object {
        public string version { get; private set; }

        public string notes { get; private set; }


        private Gee.HashMap<string, string> fields;


        public ChangelogEntry(string version) {
            this.version = version;
            this.notes = "";
            this.fields = new Gee.HashMap<string, string>();
        }


        public string get_field(string key) {
            return this.fields.get(key);
        }


        public void set_field(string key, string val) {
            this.fields.@set(key.casefold(), val);
        }

        public void add_line(string line) {
            this.notes += line + "\n";
        }

        public void finish() {
            this.notes = this.notes.strip();
        }
    }
}
